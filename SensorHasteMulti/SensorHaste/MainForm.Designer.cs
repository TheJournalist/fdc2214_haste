﻿namespace SensorHaste
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnEndereco = new System.Windows.Forms.Button();
            this.tbEndereco = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnTemp = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.lbTemp = new System.Windows.Forms.Label();
            this.lbModulo = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.btnModulo = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.cartesianChart1 = new LiveCharts.WinForms.CartesianChart();
            this.tbSensor = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.btnEscreveBaud = new System.Windows.Forms.Button();
            this.cbBaudSensor = new System.Windows.Forms.ComboBox();
            this.btnEscreverFreqExc = new System.Windows.Forms.Button();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.btnLerFreqExc = new System.Windows.Forms.Button();
            this.tbFreqExc = new System.Windows.Forms.TextBox();
            this.btnEscreverRes = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.btnLerRes = new System.Windows.Forms.Button();
            this.tbRes = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.btnEscreverFreqAmos = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.btnLerFreqAmos = new System.Windows.Forms.Button();
            this.tbFreqAmos = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cbChart = new System.Windows.Forms.CheckBox();
            this.label16 = new System.Windows.Forms.Label();
            this.cbSensores = new System.Windows.Forms.CheckedListBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnRefreshSerial = new System.Windows.Forms.Button();
            this.clbCOM = new System.Windows.Forms.CheckedListBox();
            this.btnIniciar = new System.Windows.Forms.Button();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.tbTaxa = new System.Windows.Forms.TextBox();
            this.tbTempo = new System.Windows.Forms.TextBox();
            this.btnDiretorio = new System.Windows.Forms.Button();
            this.tbSave = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.rtbLegend = new System.Windows.Forms.RichTextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.cbBaud = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.btnOpenClose = new System.Windows.Forms.Button();
            this.tabOld = new System.Windows.Forms.TabPage();
            this.btnEscreveRegs = new System.Windows.Forms.Button();
            this.tbVReg = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.btnEscreveReg = new System.Windows.Forms.Button();
            this.btnRegistradores = new System.Windows.Forms.Button();
            this.label35 = new System.Windows.Forms.Label();
            this.tbReg = new System.Windows.Forms.TextBox();
            this.btnReg = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.lbReg = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabOld.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnEndereco
            // 
            this.btnEndereco.BackColor = System.Drawing.SystemColors.Control;
            this.btnEndereco.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnEndereco.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEndereco.ForeColor = System.Drawing.Color.Black;
            this.btnEndereco.Location = new System.Drawing.Point(201, 13);
            this.btnEndereco.Margin = new System.Windows.Forms.Padding(2);
            this.btnEndereco.Name = "btnEndereco";
            this.btnEndereco.Size = new System.Drawing.Size(58, 24);
            this.btnEndereco.TabIndex = 6;
            this.btnEndereco.Text = "Escrever";
            this.btnEndereco.UseVisualStyleBackColor = false;
            this.btnEndereco.Click += new System.EventHandler(this.btnEndereco_Click);
            // 
            // tbEndereco
            // 
            this.tbEndereco.BackColor = System.Drawing.SystemColors.Control;
            this.tbEndereco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbEndereco.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbEndereco.ForeColor = System.Drawing.Color.Black;
            this.tbEndereco.Location = new System.Drawing.Point(158, 14);
            this.tbEndereco.Margin = new System.Windows.Forms.Padding(2);
            this.tbEndereco.Name = "tbEndereco";
            this.tbEndereco.Size = new System.Drawing.Size(33, 21);
            this.tbEndereco.TabIndex = 7;
            this.tbEndereco.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(24, 42);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(222, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Define endereço do sensor conectado à rede";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(24, 56);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(159, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Deve ser um valor entre 1 e 247";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(24, 15);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(129, 17);
            this.label7.TabIndex = 14;
            this.label7.Text = "Definir endereço";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(23, 379);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 17);
            this.label5.TabIndex = 19;
            this.label5.Text = "Temperatura";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(24, 405);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(255, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Valor em graus Celsius da temperatura da placa PCB";
            // 
            // btnTemp
            // 
            this.btnTemp.BackColor = System.Drawing.SystemColors.Control;
            this.btnTemp.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnTemp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTemp.ForeColor = System.Drawing.Color.Black;
            this.btnTemp.Location = new System.Drawing.Point(159, 376);
            this.btnTemp.Margin = new System.Windows.Forms.Padding(2);
            this.btnTemp.Name = "btnTemp";
            this.btnTemp.Size = new System.Drawing.Size(58, 24);
            this.btnTemp.TabIndex = 15;
            this.btnTemp.Text = "Ler";
            this.btnTemp.UseVisualStyleBackColor = false;
            this.btnTemp.Click += new System.EventHandler(this.btnTemp_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(20, 93);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 20);
            this.label6.TabIndex = 20;
            this.label6.Text = "Sensor";
            // 
            // lbTemp
            // 
            this.lbTemp.AutoSize = true;
            this.lbTemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTemp.ForeColor = System.Drawing.Color.Black;
            this.lbTemp.Location = new System.Drawing.Point(263, 370);
            this.lbTemp.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbTemp.Name = "lbTemp";
            this.lbTemp.Size = new System.Drawing.Size(53, 29);
            this.lbTemp.TabIndex = 21;
            this.lbTemp.Text = "?ºC";
            // 
            // lbModulo
            // 
            this.lbModulo.AutoSize = true;
            this.lbModulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbModulo.ForeColor = System.Drawing.Color.Black;
            this.lbModulo.Location = new System.Drawing.Point(242, 154);
            this.lbModulo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbModulo.Name = "lbModulo";
            this.lbModulo.Size = new System.Drawing.Size(26, 29);
            this.lbModulo.TabIndex = 27;
            this.lbModulo.Text = "?";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(26, 162);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(119, 17);
            this.label12.TabIndex = 25;
            this.label12.Text = "Módulo relativo";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(27, 188);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(266, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "Valor que varia de 0 a 4095 em função da impedância.";
            // 
            // btnModulo
            // 
            this.btnModulo.BackColor = System.Drawing.SystemColors.Control;
            this.btnModulo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnModulo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnModulo.ForeColor = System.Drawing.Color.Black;
            this.btnModulo.Location = new System.Drawing.Point(148, 159);
            this.btnModulo.Margin = new System.Windows.Forms.Padding(2);
            this.btnModulo.Name = "btnModulo";
            this.btnModulo.Size = new System.Drawing.Size(58, 24);
            this.btnModulo.TabIndex = 22;
            this.btnModulo.Text = "Ler";
            this.btnModulo.UseVisualStyleBackColor = false;
            this.btnModulo.Click += new System.EventHandler(this.btnModulo_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(27, 201);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(277, 13);
            this.label14.TabIndex = 28;
            this.label14.Text = "A medida corresponde a admitância e seu valor é relativo";
            // 
            // cartesianChart1
            // 
            this.cartesianChart1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cartesianChart1.Location = new System.Drawing.Point(2, 95);
            this.cartesianChart1.Margin = new System.Windows.Forms.Padding(2);
            this.cartesianChart1.Name = "cartesianChart1";
            this.cartesianChart1.Size = new System.Drawing.Size(497, 331);
            this.cartesianChart1.TabIndex = 30;
            this.cartesianChart1.Text = "cartesianChart1";
            // 
            // tbSensor
            // 
            this.tbSensor.BackColor = System.Drawing.SystemColors.Control;
            this.tbSensor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbSensor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbSensor.ForeColor = System.Drawing.Color.Black;
            this.tbSensor.Location = new System.Drawing.Point(89, 89);
            this.tbSensor.Margin = new System.Windows.Forms.Padding(2);
            this.tbSensor.Name = "tbSensor";
            this.tbSensor.Size = new System.Drawing.Size(37, 26);
            this.tbSensor.TabIndex = 16;
            this.tbSensor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(22, 68);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(295, 13);
            this.label11.TabIndex = 37;
            this.label11.Text = "________________________________________________";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(24, 237);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 17);
            this.label2.TabIndex = 41;
            this.label2.Text = "BaudRate";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(25, 262);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(263, 13);
            this.label17.TabIndex = 40;
            this.label17.Text = "Velocidade de comunicação (9600 / 38400 / 115200)";
            // 
            // btnEscreveBaud
            // 
            this.btnEscreveBaud.BackColor = System.Drawing.SystemColors.Control;
            this.btnEscreveBaud.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnEscreveBaud.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEscreveBaud.ForeColor = System.Drawing.Color.Black;
            this.btnEscreveBaud.Location = new System.Drawing.Point(175, 233);
            this.btnEscreveBaud.Margin = new System.Windows.Forms.Padding(2);
            this.btnEscreveBaud.Name = "btnEscreveBaud";
            this.btnEscreveBaud.Size = new System.Drawing.Size(58, 24);
            this.btnEscreveBaud.TabIndex = 42;
            this.btnEscreveBaud.Text = "Escrever";
            this.btnEscreveBaud.UseVisualStyleBackColor = false;
            this.btnEscreveBaud.Click += new System.EventHandler(this.btnEscreveBaud_Click);
            // 
            // cbBaudSensor
            // 
            this.cbBaudSensor.BackColor = System.Drawing.SystemColors.Control;
            this.cbBaudSensor.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cbBaudSensor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbBaudSensor.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbBaudSensor.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbBaudSensor.ForeColor = System.Drawing.Color.Black;
            this.cbBaudSensor.FormattingEnabled = true;
            this.cbBaudSensor.Items.AddRange(new object[] {
            "9600",
            "38400",
            "115200"});
            this.cbBaudSensor.Location = new System.Drawing.Point(237, 235);
            this.cbBaudSensor.Margin = new System.Windows.Forms.Padding(2);
            this.cbBaudSensor.Name = "cbBaudSensor";
            this.cbBaudSensor.Size = new System.Drawing.Size(62, 20);
            this.cbBaudSensor.TabIndex = 44;
            // 
            // btnEscreverFreqExc
            // 
            this.btnEscreverFreqExc.BackColor = System.Drawing.SystemColors.Control;
            this.btnEscreverFreqExc.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnEscreverFreqExc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEscreverFreqExc.ForeColor = System.Drawing.Color.Black;
            this.btnEscreverFreqExc.Location = new System.Drawing.Point(178, 312);
            this.btnEscreverFreqExc.Margin = new System.Windows.Forms.Padding(2);
            this.btnEscreverFreqExc.Name = "btnEscreverFreqExc";
            this.btnEscreverFreqExc.Size = new System.Drawing.Size(58, 24);
            this.btnEscreverFreqExc.TabIndex = 49;
            this.btnEscreverFreqExc.Text = "Escrever";
            this.btnEscreverFreqExc.UseVisualStyleBackColor = false;
            this.btnEscreverFreqExc.Click += new System.EventHandler(this.btnEscreverFreqExc_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Black;
            this.label18.Location = new System.Drawing.Point(27, 315);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(71, 17);
            this.label18.TabIndex = 48;
            this.label18.Text = "FreqVref";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.Black;
            this.label19.Location = new System.Drawing.Point(28, 340);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(251, 13);
            this.label19.TabIndex = 47;
            this.label19.Text = "Frequência de excitação do sinal (100 - 1000) [kHz]";
            // 
            // btnLerFreqExc
            // 
            this.btnLerFreqExc.BackColor = System.Drawing.SystemColors.Control;
            this.btnLerFreqExc.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnLerFreqExc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLerFreqExc.ForeColor = System.Drawing.Color.Black;
            this.btnLerFreqExc.Location = new System.Drawing.Point(135, 312);
            this.btnLerFreqExc.Margin = new System.Windows.Forms.Padding(2);
            this.btnLerFreqExc.Name = "btnLerFreqExc";
            this.btnLerFreqExc.Size = new System.Drawing.Size(38, 24);
            this.btnLerFreqExc.TabIndex = 46;
            this.btnLerFreqExc.Text = "Ler";
            this.btnLerFreqExc.UseVisualStyleBackColor = false;
            this.btnLerFreqExc.Click += new System.EventHandler(this.btnLerFreqExc_Click);
            // 
            // tbFreqExc
            // 
            this.tbFreqExc.BackColor = System.Drawing.SystemColors.Control;
            this.tbFreqExc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFreqExc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbFreqExc.ForeColor = System.Drawing.Color.Black;
            this.tbFreqExc.Location = new System.Drawing.Point(240, 312);
            this.tbFreqExc.Margin = new System.Windows.Forms.Padding(2);
            this.tbFreqExc.Name = "tbFreqExc";
            this.tbFreqExc.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbFreqExc.Size = new System.Drawing.Size(62, 26);
            this.tbFreqExc.TabIndex = 45;
            this.tbFreqExc.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnEscreverRes
            // 
            this.btnEscreverRes.BackColor = System.Drawing.SystemColors.Control;
            this.btnEscreverRes.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnEscreverRes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEscreverRes.ForeColor = System.Drawing.Color.Black;
            this.btnEscreverRes.Location = new System.Drawing.Point(202, 532);
            this.btnEscreverRes.Margin = new System.Windows.Forms.Padding(2);
            this.btnEscreverRes.Name = "btnEscreverRes";
            this.btnEscreverRes.Size = new System.Drawing.Size(58, 24);
            this.btnEscreverRes.TabIndex = 54;
            this.btnEscreverRes.Text = "Escrever";
            this.btnEscreverRes.UseVisualStyleBackColor = false;
            this.btnEscreverRes.Click += new System.EventHandler(this.btnEscreverRes_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Black;
            this.label20.Location = new System.Drawing.Point(23, 535);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(79, 17);
            this.label20.TabIndex = 53;
            this.label20.Text = "ResCarga";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Black;
            this.label21.Location = new System.Drawing.Point(24, 560);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(255, 13);
            this.label21.TabIndex = 52;
            this.label21.Text = "O modelo elétrico corresponde a um resistor em série";
            // 
            // btnLerRes
            // 
            this.btnLerRes.BackColor = System.Drawing.SystemColors.Control;
            this.btnLerRes.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnLerRes.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLerRes.ForeColor = System.Drawing.Color.Black;
            this.btnLerRes.Location = new System.Drawing.Point(159, 532);
            this.btnLerRes.Margin = new System.Windows.Forms.Padding(2);
            this.btnLerRes.Name = "btnLerRes";
            this.btnLerRes.Size = new System.Drawing.Size(38, 24);
            this.btnLerRes.TabIndex = 51;
            this.btnLerRes.Text = "Ler";
            this.btnLerRes.UseVisualStyleBackColor = false;
            this.btnLerRes.Click += new System.EventHandler(this.btnLerRes_Click);
            // 
            // tbRes
            // 
            this.tbRes.BackColor = System.Drawing.SystemColors.Control;
            this.tbRes.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbRes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbRes.ForeColor = System.Drawing.Color.Black;
            this.tbRes.Location = new System.Drawing.Point(264, 532);
            this.tbRes.Margin = new System.Windows.Forms.Padding(2);
            this.tbRes.Name = "tbRes";
            this.tbRes.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbRes.Size = new System.Drawing.Size(52, 26);
            this.tbRes.TabIndex = 50;
            this.tbRes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Black;
            this.label22.Location = new System.Drawing.Point(24, 574);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(260, 13);
            this.label22.TabIndex = 55;
            this.label22.Text = "com o sensor. O valor deste resistor pode ser alterado";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Black;
            this.label23.Location = new System.Drawing.Point(24, 586);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(214, 13);
            this.label23.TabIndex = 56;
            this.label23.Text = "tendo as opções de 100 ohms e 3300 ohms";
            // 
            // btnEscreverFreqAmos
            // 
            this.btnEscreverFreqAmos.BackColor = System.Drawing.SystemColors.Control;
            this.btnEscreverFreqAmos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnEscreverFreqAmos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEscreverFreqAmos.ForeColor = System.Drawing.Color.Black;
            this.btnEscreverFreqAmos.Location = new System.Drawing.Point(202, 454);
            this.btnEscreverFreqAmos.Margin = new System.Windows.Forms.Padding(2);
            this.btnEscreverFreqAmos.Name = "btnEscreverFreqAmos";
            this.btnEscreverFreqAmos.Size = new System.Drawing.Size(58, 24);
            this.btnEscreverFreqAmos.TabIndex = 61;
            this.btnEscreverFreqAmos.Text = "Escrever";
            this.btnEscreverFreqAmos.UseVisualStyleBackColor = false;
            this.btnEscreverFreqAmos.Click += new System.EventHandler(this.btnEscreverFreqAmos_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            this.label24.ForeColor = System.Drawing.Color.Black;
            this.label24.Location = new System.Drawing.Point(23, 460);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(130, 17);
            this.label24.TabIndex = 60;
            this.label24.Text = "FreqAmostragem";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Black;
            this.label25.Location = new System.Drawing.Point(24, 485);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(256, 26);
            this.label25.TabIndex = 59;
            this.label25.Text = "Frequencia com que o sinal do transdutor é lido pelo \r\nmicrocontrolador (1 a 2000" +
    " amostras/s)\r\n";
            // 
            // btnLerFreqAmos
            // 
            this.btnLerFreqAmos.BackColor = System.Drawing.SystemColors.Control;
            this.btnLerFreqAmos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnLerFreqAmos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLerFreqAmos.ForeColor = System.Drawing.Color.Black;
            this.btnLerFreqAmos.Location = new System.Drawing.Point(159, 454);
            this.btnLerFreqAmos.Margin = new System.Windows.Forms.Padding(2);
            this.btnLerFreqAmos.Name = "btnLerFreqAmos";
            this.btnLerFreqAmos.Size = new System.Drawing.Size(38, 24);
            this.btnLerFreqAmos.TabIndex = 58;
            this.btnLerFreqAmos.Text = "Ler";
            this.btnLerFreqAmos.UseVisualStyleBackColor = false;
            this.btnLerFreqAmos.Click += new System.EventHandler(this.btnLerFreqAmos_Click);
            // 
            // tbFreqAmos
            // 
            this.tbFreqAmos.BackColor = System.Drawing.SystemColors.Control;
            this.tbFreqAmos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbFreqAmos.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbFreqAmos.ForeColor = System.Drawing.Color.Black;
            this.tbFreqAmos.Location = new System.Drawing.Point(264, 454);
            this.tbFreqAmos.Margin = new System.Windows.Forms.Padding(2);
            this.tbFreqAmos.Name = "tbFreqAmos";
            this.tbFreqAmos.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbFreqAmos.Size = new System.Drawing.Size(52, 26);
            this.tbFreqAmos.TabIndex = 57;
            this.tbFreqAmos.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(23, 288);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(295, 13);
            this.label26.TabIndex = 62;
            this.label26.Text = "________________________________________________";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.Black;
            this.label27.Location = new System.Drawing.Point(27, 212);
            this.label27.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(295, 13);
            this.label27.TabIndex = 63;
            this.label27.Text = "________________________________________________";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(22, 432);
            this.label28.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(295, 13);
            this.label28.TabIndex = 64;
            this.label28.Text = "________________________________________________";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Black;
            this.label30.Location = new System.Drawing.Point(21, 510);
            this.label30.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(295, 13);
            this.label30.TabIndex = 66;
            this.label30.Text = "________________________________________________";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabOld);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(518, 647);
            this.tabControl1.TabIndex = 67;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(510, 621);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Medições";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.cbChart);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.cartesianChart1);
            this.panel2.Controls.Add(this.cbSensores);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 188);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(504, 430);
            this.panel2.TabIndex = 59;
            // 
            // cbChart
            // 
            this.cbChart.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbChart.AutoSize = true;
            this.cbChart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbChart.ForeColor = System.Drawing.Color.Black;
            this.cbChart.Location = new System.Drawing.Point(242, 41);
            this.cbChart.Margin = new System.Windows.Forms.Padding(2);
            this.cbChart.Name = "cbChart";
            this.cbChart.Size = new System.Drawing.Size(32, 23);
            this.cbChart.TabIndex = 55;
            this.cbChart.Text = "Ler";
            this.cbChart.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(12, 10);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(55, 16);
            this.label16.TabIndex = 56;
            this.label16.Text = "Leitura";
            // 
            // cbSensores
            // 
            this.cbSensores.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.cbSensores.FormattingEnabled = true;
            this.cbSensores.Location = new System.Drawing.Point(88, 11);
            this.cbSensores.Name = "cbSensores";
            this.cbSensores.Size = new System.Drawing.Size(149, 75);
            this.cbSensores.TabIndex = 52;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnRefreshSerial);
            this.panel1.Controls.Add(this.clbCOM);
            this.panel1.Controls.Add(this.btnIniciar);
            this.panel1.Controls.Add(this.label32);
            this.panel1.Controls.Add(this.label31);
            this.panel1.Controls.Add(this.tbTaxa);
            this.panel1.Controls.Add(this.tbTempo);
            this.panel1.Controls.Add(this.btnDiretorio);
            this.panel1.Controls.Add(this.tbSave);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.rtbLegend);
            this.panel1.Controls.Add(this.label29);
            this.panel1.Controls.Add(this.cbBaud);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.btnOpenClose);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(504, 185);
            this.panel1.TabIndex = 58;
            // 
            // btnRefreshSerial
            // 
            this.btnRefreshSerial.Location = new System.Drawing.Point(204, 72);
            this.btnRefreshSerial.Name = "btnRefreshSerial";
            this.btnRefreshSerial.Size = new System.Drawing.Size(20, 20);
            this.btnRefreshSerial.TabIndex = 69;
            this.btnRefreshSerial.UseVisualStyleBackColor = true;
            this.btnRefreshSerial.Click += new System.EventHandler(this.btnRefreshSerial_Click);
            // 
            // clbCOM
            // 
            this.clbCOM.FormattingEnabled = true;
            this.clbCOM.Location = new System.Drawing.Point(14, 32);
            this.clbCOM.Name = "clbCOM";
            this.clbCOM.Size = new System.Drawing.Size(89, 34);
            this.clbCOM.TabIndex = 68;
            this.clbCOM.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.clbCOM_ItemCheck);
            this.clbCOM.SelectedIndexChanged += new System.EventHandler(this.clbCOM_SelectedIndexChanged);
            // 
            // btnIniciar
            // 
            this.btnIniciar.Location = new System.Drawing.Point(391, 111);
            this.btnIniciar.Name = "btnIniciar";
            this.btnIniciar.Size = new System.Drawing.Size(75, 23);
            this.btnIniciar.TabIndex = 67;
            this.btnIniciar.Text = "Iniciar";
            this.btnIniciar.UseVisualStyleBackColor = true;
            this.btnIniciar.Click += new System.EventHandler(this.btnIniciar_Click);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(472, 85);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(26, 13);
            this.label32.TabIndex = 66;
            this.label32.Text = "[Hz]";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(472, 59);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(18, 13);
            this.label31.TabIndex = 65;
            this.label31.Text = "[s]";
            // 
            // tbTaxa
            // 
            this.tbTaxa.Location = new System.Drawing.Point(391, 82);
            this.tbTaxa.Name = "tbTaxa";
            this.tbTaxa.Size = new System.Drawing.Size(75, 20);
            this.tbTaxa.TabIndex = 64;
            this.tbTaxa.Text = "1";
            // 
            // tbTempo
            // 
            this.tbTempo.Location = new System.Drawing.Point(391, 56);
            this.tbTempo.Name = "tbTempo";
            this.tbTempo.Size = new System.Drawing.Size(75, 20);
            this.tbTempo.TabIndex = 63;
            this.tbTempo.Text = "10";
            // 
            // btnDiretorio
            // 
            this.btnDiretorio.Location = new System.Drawing.Point(391, 27);
            this.btnDiretorio.Name = "btnDiretorio";
            this.btnDiretorio.Size = new System.Drawing.Size(75, 23);
            this.btnDiretorio.TabIndex = 62;
            this.btnDiretorio.Text = "Diretório";
            this.btnDiretorio.UseVisualStyleBackColor = true;
            this.btnDiretorio.Click += new System.EventHandler(this.btnDiretorio_Click);
            // 
            // tbSave
            // 
            this.tbSave.Location = new System.Drawing.Point(269, 29);
            this.tbSave.Name = "tbSave";
            this.tbSave.ReadOnly = true;
            this.tbSave.Size = new System.Drawing.Size(116, 20);
            this.tbSave.TabIndex = 61;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(351, 85);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(34, 13);
            this.label15.TabIndex = 60;
            this.label15.Text = "Taxa:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(342, 59);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 59;
            this.label10.Text = "Tempo:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(266, 10);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 16);
            this.label9.TabIndex = 58;
            this.label9.Text = "Aquisição";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(11, 10);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 16);
            this.label1.TabIndex = 46;
            this.label1.Text = "Porta Serial";
            // 
            // rtbLegend
            // 
            this.rtbLegend.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtbLegend.Location = new System.Drawing.Point(88, 100);
            this.rtbLegend.Name = "rtbLegend";
            this.rtbLegend.Size = new System.Drawing.Size(149, 79);
            this.rtbLegend.TabIndex = 57;
            this.rtbLegend.Text = "";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(11, 72);
            this.label29.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(135, 16);
            this.label29.TabIndex = 48;
            this.label29.Text = "Detectar sensores";
            // 
            // cbBaud
            // 
            this.cbBaud.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbBaud.FormattingEnabled = true;
            this.cbBaud.Items.AddRange(new object[] {
            "9600",
            "38400",
            "115200"});
            this.cbBaud.Location = new System.Drawing.Point(109, 37);
            this.cbBaud.Name = "cbBaud";
            this.cbBaud.Size = new System.Drawing.Size(64, 21);
            this.cbBaud.TabIndex = 51;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(14, 100);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(58, 26);
            this.button1.TabIndex = 54;
            this.button1.Text = "Detectar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.btnDetectar_Click);
            // 
            // btnOpenClose
            // 
            this.btnOpenClose.Location = new System.Drawing.Point(179, 37);
            this.btnOpenClose.Name = "btnOpenClose";
            this.btnOpenClose.Size = new System.Drawing.Size(58, 23);
            this.btnOpenClose.TabIndex = 53;
            this.btnOpenClose.Text = "Abrir";
            this.btnOpenClose.UseVisualStyleBackColor = true;
            this.btnOpenClose.Click += new System.EventHandler(this.btnOpenClose_Click);
            // 
            // tabOld
            // 
            this.tabOld.Controls.Add(this.btnEscreveRegs);
            this.tabOld.Controls.Add(this.tbVReg);
            this.tabOld.Controls.Add(this.label34);
            this.tabOld.Controls.Add(this.btnEscreveReg);
            this.tabOld.Controls.Add(this.btnRegistradores);
            this.tabOld.Controls.Add(this.label35);
            this.tabOld.Controls.Add(this.tbReg);
            this.tabOld.Controls.Add(this.btnReg);
            this.tabOld.Controls.Add(this.label33);
            this.tabOld.Controls.Add(this.lbReg);
            this.tabOld.Controls.Add(this.label30);
            this.tabOld.Controls.Add(this.label28);
            this.tabOld.Controls.Add(this.label27);
            this.tabOld.Controls.Add(this.btnEndereco);
            this.tabOld.Controls.Add(this.label26);
            this.tabOld.Controls.Add(this.tbEndereco);
            this.tabOld.Controls.Add(this.btnEscreverFreqAmos);
            this.tabOld.Controls.Add(this.label3);
            this.tabOld.Controls.Add(this.label24);
            this.tabOld.Controls.Add(this.label4);
            this.tabOld.Controls.Add(this.label25);
            this.tabOld.Controls.Add(this.label7);
            this.tabOld.Controls.Add(this.btnLerFreqAmos);
            this.tabOld.Controls.Add(this.btnTemp);
            this.tabOld.Controls.Add(this.tbFreqAmos);
            this.tabOld.Controls.Add(this.tbSensor);
            this.tabOld.Controls.Add(this.label23);
            this.tabOld.Controls.Add(this.label8);
            this.tabOld.Controls.Add(this.label22);
            this.tabOld.Controls.Add(this.label5);
            this.tabOld.Controls.Add(this.btnEscreverRes);
            this.tabOld.Controls.Add(this.label6);
            this.tabOld.Controls.Add(this.label20);
            this.tabOld.Controls.Add(this.lbTemp);
            this.tabOld.Controls.Add(this.label21);
            this.tabOld.Controls.Add(this.btnModulo);
            this.tabOld.Controls.Add(this.btnLerRes);
            this.tabOld.Controls.Add(this.label13);
            this.tabOld.Controls.Add(this.tbRes);
            this.tabOld.Controls.Add(this.label12);
            this.tabOld.Controls.Add(this.btnEscreverFreqExc);
            this.tabOld.Controls.Add(this.lbModulo);
            this.tabOld.Controls.Add(this.label18);
            this.tabOld.Controls.Add(this.label14);
            this.tabOld.Controls.Add(this.label19);
            this.tabOld.Controls.Add(this.btnLerFreqExc);
            this.tabOld.Controls.Add(this.tbFreqExc);
            this.tabOld.Controls.Add(this.cbBaudSensor);
            this.tabOld.Controls.Add(this.btnEscreveBaud);
            this.tabOld.Controls.Add(this.label2);
            this.tabOld.Controls.Add(this.label11);
            this.tabOld.Controls.Add(this.label17);
            this.tabOld.Location = new System.Drawing.Point(4, 22);
            this.tabOld.Name = "tabOld";
            this.tabOld.Padding = new System.Windows.Forms.Padding(3);
            this.tabOld.Size = new System.Drawing.Size(510, 621);
            this.tabOld.TabIndex = 1;
            this.tabOld.Text = "Configuração";
            this.tabOld.UseVisualStyleBackColor = true;
            // 
            // btnEscreveRegs
            // 
            this.btnEscreveRegs.Location = new System.Drawing.Point(355, 207);
            this.btnEscreveRegs.Name = "btnEscreveRegs";
            this.btnEscreveRegs.Size = new System.Drawing.Size(111, 37);
            this.btnEscreveRegs.TabIndex = 76;
            this.btnEscreveRegs.Text = "Escreve regs.";
            this.btnEscreveRegs.UseVisualStyleBackColor = true;
            this.btnEscreveRegs.Click += new System.EventHandler(this.btnEscreveRegs_Click);
            // 
            // tbVReg
            // 
            this.tbVReg.BackColor = System.Drawing.SystemColors.Control;
            this.tbVReg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbVReg.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbVReg.ForeColor = System.Drawing.Color.Black;
            this.tbVReg.Location = new System.Drawing.Point(344, 116);
            this.tbVReg.Margin = new System.Windows.Forms.Padding(2);
            this.tbVReg.Name = "tbVReg";
            this.tbVReg.Size = new System.Drawing.Size(51, 26);
            this.tbVReg.TabIndex = 75;
            this.tbVReg.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.ForeColor = System.Drawing.Color.Black;
            this.label34.Location = new System.Drawing.Point(21, 356);
            this.label34.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(295, 13);
            this.label34.TabIndex = 74;
            this.label34.Text = "________________________________________________";
            // 
            // btnEscreveReg
            // 
            this.btnEscreveReg.BackColor = System.Drawing.SystemColors.Control;
            this.btnEscreveReg.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnEscreveReg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEscreveReg.ForeColor = System.Drawing.Color.Black;
            this.btnEscreveReg.Location = new System.Drawing.Point(399, 117);
            this.btnEscreveReg.Margin = new System.Windows.Forms.Padding(2);
            this.btnEscreveReg.Name = "btnEscreveReg";
            this.btnEscreveReg.Size = new System.Drawing.Size(67, 24);
            this.btnEscreveReg.TabIndex = 73;
            this.btnEscreveReg.Text = "Escrever";
            this.btnEscreveReg.UseVisualStyleBackColor = false;
            this.btnEscreveReg.Click += new System.EventHandler(this.btnEscreveReg_Click);
            // 
            // btnRegistradores
            // 
            this.btnRegistradores.Location = new System.Drawing.Point(355, 164);
            this.btnRegistradores.Name = "btnRegistradores";
            this.btnRegistradores.Size = new System.Drawing.Size(111, 37);
            this.btnRegistradores.TabIndex = 72;
            this.btnRegistradores.Text = "Lê registradores";
            this.btnRegistradores.UseVisualStyleBackColor = true;
            this.btnRegistradores.Click += new System.EventHandler(this.btnRegistradores_Click);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.ForeColor = System.Drawing.Color.Black;
            this.label35.Location = new System.Drawing.Point(21, 143);
            this.label35.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(295, 13);
            this.label35.TabIndex = 71;
            this.label35.Text = "________________________________________________";
            // 
            // tbReg
            // 
            this.tbReg.BackColor = System.Drawing.SystemColors.Control;
            this.tbReg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbReg.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbReg.ForeColor = System.Drawing.Color.Black;
            this.tbReg.Location = new System.Drawing.Point(165, 117);
            this.tbReg.Margin = new System.Windows.Forms.Padding(2);
            this.tbReg.Name = "tbReg";
            this.tbReg.Size = new System.Drawing.Size(41, 26);
            this.tbReg.TabIndex = 70;
            this.tbReg.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnReg
            // 
            this.btnReg.BackColor = System.Drawing.SystemColors.Control;
            this.btnReg.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.btnReg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReg.ForeColor = System.Drawing.Color.Black;
            this.btnReg.Location = new System.Drawing.Point(210, 118);
            this.btnReg.Margin = new System.Windows.Forms.Padding(2);
            this.btnReg.Name = "btnReg";
            this.btnReg.Size = new System.Drawing.Size(58, 24);
            this.btnReg.TabIndex = 67;
            this.btnReg.Text = "Ler";
            this.btnReg.UseVisualStyleBackColor = false;
            this.btnReg.Click += new System.EventHandler(this.btnReg_Click);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.Black;
            this.label33.Location = new System.Drawing.Point(22, 121);
            this.label33.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(139, 17);
            this.label33.TabIndex = 68;
            this.label33.Text = "Outro registrador:";
            // 
            // lbReg
            // 
            this.lbReg.AutoSize = true;
            this.lbReg.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbReg.ForeColor = System.Drawing.Color.Black;
            this.lbReg.Location = new System.Drawing.Point(273, 115);
            this.lbReg.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbReg.Name = "lbReg";
            this.lbReg.Size = new System.Drawing.Size(26, 29);
            this.lbReg.TabIndex = 69;
            this.lbReg.Text = "?";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(518, 647);
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MinimumSize = new System.Drawing.Size(534, 686);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sensor Haste";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabOld.ResumeLayout(false);
            this.tabOld.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnEndereco;
        private System.Windows.Forms.TextBox tbEndereco;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnTemp;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbTemp;
        private System.Windows.Forms.Label lbModulo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnModulo;
        private System.Windows.Forms.Label label14;
        private LiveCharts.WinForms.CartesianChart cartesianChart1;
        private System.Windows.Forms.TextBox tbSensor;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnEscreveBaud;
        private System.Windows.Forms.ComboBox cbBaudSensor;
        private System.Windows.Forms.Button btnEscreverFreqExc;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button btnLerFreqExc;
        private System.Windows.Forms.TextBox tbFreqExc;
        private System.Windows.Forms.Button btnEscreverRes;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button btnLerRes;
        private System.Windows.Forms.TextBox tbRes;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button btnEscreverFreqAmos;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Button btnLerFreqAmos;
        private System.Windows.Forms.TextBox tbFreqAmos;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabOld;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.ComboBox cbBaud;
        private System.Windows.Forms.CheckedListBox cbSensores;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnOpenClose;
        private System.Windows.Forms.CheckBox cbChart;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.RichTextBox rtbLegend;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnIniciar;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox tbTaxa;
        private System.Windows.Forms.TextBox tbTempo;
        private System.Windows.Forms.Button btnDiretorio;
        private System.Windows.Forms.TextBox tbSave;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox tbReg;
        private System.Windows.Forms.Button btnReg;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label lbReg;
        private System.Windows.Forms.Button btnRegistradores;
        private System.Windows.Forms.TextBox tbVReg;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Button btnEscreveReg;
        private System.Windows.Forms.Button btnEscreveRegs;
        private System.Windows.Forms.CheckedListBox clbCOM;
        private System.Windows.Forms.Button btnRefreshSerial;
    }
}

