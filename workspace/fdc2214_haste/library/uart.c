#include "uart.h"

#define USCI_INPUT_CLK      (24000000UL)  // in Hz
#define USCI_BAUD_RATE      (115200)

#define USCI_DIV_INT              (USCI_INPUT_CLK/USCI_BAUD_RATE)
#define USCI_BR0_VAL              (USCI_DIV_INT & 0x00FF)
#define USCI_BR1_VAL              ((USCI_DIV_INT >> 8) & 0xFF)

#define USCI_DIV_FRAC_NUMERATOR   (USCI_INPUT_CLK - (USCI_DIV_INT*USCI_BAUD_RATE))
#define USCI_DIV_FRAC_NUM_X_8     (USCI_DIV_FRAC_NUMERATOR*8)
#define USCI_DIV_FRAC_X_8         (USCI_DIV_FRAC_NUM_X_8/USCI_BAUD_RATE)

#if (((USCI_DIV_FRAC_NUM_X_8-(USCI_DIV_FRAC_X_8*USCI_BAUD_RATE))*10)/USCI_BAUD_RATE < 5)
#define USCI_BRS_VAL              (USCI_DIV_FRAC_X_8<< 1)
#else
#define USCI_BRS_VAL              ((USCI_DIV_FRAC_X_8+1)<< 1)
#endif


// RX FIFO
uint8_t RxFifo[FIFO_SIZE];
uint8_t RxFifoHead, RxFifoTail;


void uartInit()
{
    //------------ Configuring MAX485 Control Lines ---------------//
    P2DIR |= BIT4 + BIT5; // P2.5 -> DE, P2.4 -> ~RE output
    P2OUT &= ~BIT4;       // P2.4 = 0
    P2OUT &= ~BIT5;       // P2.5 = 0

    //--------- Setting the UART function for P1.1 & P1.2 ---------//
    P3SEL |= BIT3 + BIT4;           // P3.4 UCA0RXD input,  P3.3 UCA0TXD output

    //------------ Configuring the UART(USCI_A0) ----------------//
    UCA0CTL1 |= UCSSEL_2 + UCSWRST; // SMCLK
    UCA0BR0 = USCI_BR0_VAL;
    UCA0BR1 = USCI_BR1_VAL;
    UCA0MCTL = USCI_BRS_VAL;
    UCA0CTL1 &= ~UCSWRST;

    //---------------- Enabling the interrupts ------------------//
    UCA0IE |= UCRXIE; //Enable RX interrupt
    _BIS_SR(GIE);                     // Enable the global interrupt


    //--------------------- Init FIFO -------------------------//
    fifoInit(&RxFifoHead, &RxFifoTail);
}

void uartSend(char *data, unsigned char length)
{
    uint16_t i;

    //----------------- Put MAX485 in Transmit Mode ---------------//
    P2OUT |= BIT4 + BIT5;  // P2.4 ->  DE high, P2.5 -> ~RE high

    for(i=0; i<length; i++)
    {
        // Wait for TX buffer to be ready for new data
        while (!(UCA0IFG & UCTXIFG))
            ;

        // Push data to TX buffer
        UCA0TXBUF = data[i];
    }

    // Wait until the last byte is completely sent
    while (UCA0STAT & UCBUSY)
        ;

    //----------------- Put MAX485 in Receive Mode ---------------//
    P2OUT &= ~BIT4;  // P2.0 ->  DE Low
    P2OUT &= ~BIT5;  // P2.1 -> ~RE Low
}

void uartSendString(char *data)
{
    uint16_t i = 0;

    while(1)
    {
        uartSend(data+i, 1);
        if(data[i] == '\n')
            break;
        i++;
    }
}

void fifoInit(uint8_t *fifoHead, uint8_t *fifoTail)
{
    (*fifoHead) = (*fifoTail) = 0;
}

int fifoPut(uint8_t new, uint8_t (*fifo)[FIFO_SIZE], uint8_t *fifoHead, uint8_t *fifoTail)
{
    if((*fifoHead) == (( (*fifoTail) - 1 + FIFO_SIZE) % FIFO_SIZE))
    {
        return -1; /* Queue Full*/
    }

    (*fifo)[(*fifoHead)] = new;

    (*fifoHead) = ((*fifoHead) + 1) % FIFO_SIZE;

    return 0; // No errors
}

int fifoGet(uint8_t *old, uint8_t (*fifo)[FIFO_SIZE], uint8_t *fifoHead, uint8_t *fifoTail)
{
    if((*fifoHead) == (*fifoTail))
    {
        return -1; /* Queue Empty - nothing to get*/
    }

    *old = (*fifo)[(*fifoTail)];

    (*fifoTail) = ((*fifoTail) + 1) % FIFO_SIZE;

    return 0; // No errors
}


//-----------------------------------------------------------------------//
//                       Receive  interrupt                              //
//-----------------------------------------------------------------------//

#pragma vector=USCI_A0_VECTOR
__interrupt void USCI_A0_ISR(void)
{
    fifoPut(UCA0RXBUF, &RxFifo, &RxFifoHead, &RxFifoTail);
}
