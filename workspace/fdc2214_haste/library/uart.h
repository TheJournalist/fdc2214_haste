
#ifndef UART_H_
#define UART_H_

#include <stdint.h>
#include <msp430.h>

#define FIFO_ELEMENTS 128
#define FIFO_SIZE (FIFO_ELEMENTS + 1)

// RX FIFO
extern uint8_t RxFifo[FIFO_SIZE];
extern uint8_t RxFifoHead, RxFifoTail;

// FDC FIFO
extern uint8_t FdcFifo[FIFO_SIZE];
extern uint8_t FdcFifoHead, FdcFifoTail;

void uartInit();
void uartSend(char *data, unsigned char length);
void uartSendString(char *data);

void fifoInit(uint8_t *fifoHead, uint8_t *fifoTail);
int fifoPut(uint8_t new, uint8_t (*fifo)[FIFO_SIZE], uint8_t *fifoHead, uint8_t *fifoTail);
int fifoGet(uint8_t *old, uint8_t (*fifo)[FIFO_SIZE], uint8_t *fifoHead, uint8_t *fifoTail);

#endif /* UART_H_ */
