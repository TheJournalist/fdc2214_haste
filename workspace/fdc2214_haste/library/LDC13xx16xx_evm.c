/* Copyright (C) 2015 Texas Instruments Incorporated - http://www.ti.com/  ALL RIGHTS RESERVED  */

#include "LDC13xx16xx_cmd.h"
#include "LDC13xx16xx_evm.h"
#include "HAL_UCS.h"
#include "HAL_PMM.h"
#include "driverlib/MSP430F5xx_6xx/driverlib.h"
#include "pll.h"
#include "i2c.h"
#include "crc8.h"

#include "HAL_FLASH.h"

/** Defaults */
#define INFOD_START (0x1800)
#pragma DATA_SECTION(FlashConfig, ".infoD")
uint16_t FlashConfig[EVM_DEFAULTS_SIZE * sizeof(uint16_t)];

/** DRDY */
volatile uint8_t dataReady;
static uint16_t allData[8];
uint8_t default_addr;

/** EVM */
uint8_t evm_device;
uint8_t evm_rev;
uint8_t evm_device_series;
uint8_t evm_device_channels;

uint8_t EVM_STR_FDC2214[] = { 'F', 'D', 'C', '2', '2', '1', '4', 'E', 'V', 'M',
                              '\n' };

void evm_Delay_Ms(uint16_t ms)
{
    uint16_t i;
    for (i = 0; i < ms; i++)
    {
        __delay_cycles(EVM_TIME_1MS);
    }
}

void InitMCU(void);

/** Initialization */
uint8_t fdc2214_init()
{
    uint8_t retVal = 1;
    dataReady = 0;

    _disable_interrupts();

    // Set device I2C address to 0x2A (ADDR low)
    EVM_ADDR_DIR |= EVM_ADDR_BIT;
    EVM_ADDR_OUT &= ~EVM_ADDR_BIT;
    default_addr = EVM_DEFAULT_I2CADDR;

    // Take the device out of shutdown mode and wait 10ms
    EVM_SHUTDOWN_DIR |= EVM_SHUTDOWN_BIT;
    EVM_SHUTDOWN_OUT |= EVM_SHUTDOWN_BIT;
    evm_Delay_Ms(10);
    EVM_SHUTDOWN_OUT &= ~EVM_SHUTDOWN_BIT;
    evm_Delay_Ms(10);

    _enable_interrupts();

    // software reset
    smbus_writeWord(default_addr, LDC13xx16xx_CMD_RESET_DEVICE, 0x8000);
    evm_Delay_Ms(10);

    // Config
    retVal &= smbus_writeWord(default_addr, LDC13xx16xx_CMD_REF_COUNT_CH0, 0x04D5);

    retVal &= smbus_writeWord(default_addr, LDC13xx16xx_CMD_SETTLE_COUNT_CH0, 0x000A);

    retVal &= smbus_writeWord(default_addr, LDC13xx16xx_CMD_CLOCK_DIVIDERS_CH0, 0x2001);

    // I Drive should be decimal 15
    retVal &= smbus_writeWord(default_addr, LDC13xx16xx_CMD_DRIVE_CURRENT_CH0, 0x7C00);

    // report only DRDYs to INT
    retVal &= smbus_writeWord(default_addr, LDC13xx16xx_CMD_ERROR_CONFIG, 0x0001);

    // autoscan ACTIVE_CHAN, deglitch 10Mhz
    retVal &= smbus_writeWord(default_addr, LDC13xx16xx_CMD_MUX_CONFIG, 0x020D);

    // Perform continuous conversions on Channel 0 / Device is active /
    // Full Current Activation Mode / Use Internal oscillator as reference frequency /
    // INTB pin will be asserted when status register updates /
    // The FDC will drive channel 0 with current >1.5mA
    retVal &= smbus_writeWord(default_addr, LDC13xx16xx_CMD_CONFIG, 0x1401);
    evm_Delay_Ms(10);


    // Check if the device was not properly set
    if (evm_device == 0)
    {
        evm_rev = EVM_REV_A;

        // Get the device ID
        uint16_t dev_id;
        smbus_readWord(default_addr, LDC13xx16xx_CMD_DEVID, &dev_id);

        // Check if 13 or 16 series
        if (dev_id == LDC13xx_DEV_ID)
        {
            evm_device_series = 13;
        }
        else
        {
            // Assume series 16 device if not detected
            evm_device_series = 16;
        }

        // Assume that a four channel device is attached
        evm_device_channels = 4;

    }

    // setup DRDY pin interrupt
    EVM_INT_DIR &= ~EVM_INT_BIT;						// INPUT
    EVM_INT_IE |= EVM_INT_BIT;							// interrupt enabled
    EVM_INT_IES |= EVM_INT_BIT;							// Hi->Lo Edge
    EVM_INT_IFG &= ~EVM_INT_BIT;						// Clear IFG

    return retVal;
}

// TODO: use DMA to transfer data
// interrupt service routine, DRDY
#pragma vector=EVM_INT_VECTOR
__interrupt void DRDY()
{
    if (!(EVM_INT_IN & EVM_INT_BIT))
    {
        dataReady = 1;
    }
    EVM_INT_IFG &= ~EVM_INT_BIT;       // IFG cleared
}

void evm_processDRDY()
{
    if (dataReady)
    {
        smbus_readWord(default_addr, LDC13xx16xx_CMD_DATA_MSB_CH0, &allData[0]);
        smbus_readWord(default_addr, LDC13xx16xx_CMD_DATA_LSB_CH0, &allData[1]);
        smbus_readWord(default_addr, LDC13xx16xx_CMD_DATA_MSB_CH1, &allData[2]);
        smbus_readWord(default_addr, LDC13xx16xx_CMD_DATA_LSB_CH1, &allData[3]);
        smbus_readWord(default_addr, LDC13xx16xx_CMD_DATA_MSB_CH2, &allData[4]);
        smbus_readWord(default_addr, LDC13xx16xx_CMD_DATA_LSB_CH2, &allData[5]);
        smbus_readWord(default_addr, LDC13xx16xx_CMD_DATA_MSB_CH3, &allData[6]);
        smbus_readWord(default_addr, LDC13xx16xx_CMD_DATA_LSB_CH3, &allData[7]);
        dataReady = 0;
    }
}

// TODO: use DMA to transfer data
// ch 0-3
uint8_t evm_readFreq(uint8_t ch, uint8_t * buffer)
{
    buffer[0] = allData[ch * 2] >> 8;
    buffer[1] = allData[ch * 2] & 0xFF;
    buffer[2] = allData[ch * 2 + 1] >> 8;
    buffer[3] = allData[ch * 2 + 1] & 0xFF;
    return 4;
}

uint8_t evm_changeAddr(uint8_t addr)
{
    if (default_addr == addr)
        return default_addr;
    if (addr == EVM_MAX_I2CADDR)
    {
        EVM_ADDR_OUT |= EVM_ADDR_BIT;
        default_addr = EVM_MAX_I2CADDR;
    }
    else if (addr == EVM_MIN_I2CADDR)
    {
        EVM_ADDR_OUT &= ~EVM_ADDR_BIT;
        default_addr = EVM_MIN_I2CADDR;
    }
    return default_addr;
}

void init_Clock_prePLL()
{

    UCSCTL3 |= SELREF_2;                      // Set DCO FLL reference = REFO
    UCSCTL4 |= SELA_3;                        // Set ACLK = REFO

    __bis_SR_register(SCG0);                  // Disable the FLL control loop
    UCSCTL0 = 0x0000;                         // Set lowest possible DCOx, MODx
    UCSCTL1 = DCORSEL_5;                     // Select DCO range 24MHz operation
    UCSCTL2 = FLLD_1 + 374;                   // Set DCO Multiplier for 12MHz
                                              // (N + 1) * FLLRef = Fdco
                                              // ( 374+1) * 32k = 12MHz
                                              // Set FLL Div = fDCOCLK/2
    __bic_SR_register(SCG0);                  // Enable the FLL control loop

    // Worst-case settling time for the DCO when the DCO range bits have been
    // changed is n x 32 x 32 x f_MCLK / f_FLL_reference. See UCS chapter in 5xx
    // UG for optimization.
    // 32 x 32 x 12 MHz / 32,768 Hz = 375000 = MCLK cycles for DCO to settle
    __delay_cycles(375000);

    // Loop until XT1,XT2 & DCO fault flag is cleared
    do
    {
        UCSCTL7 &= ~(XT2OFFG + XT1LFOFFG + DCOFFG);
        // Clear XT2,XT1,DCO fault flags
        SFRIFG1 &= ~OFIFG;                      // Clear fault flags
    }
    while (SFRIFG1 & OFIFG);                   // Test oscillator fault flag
}

/** Initialze MCU
 Initializes the MSP430 peripherals and modules.
 */
void InitMCU(void)
{
    i2c_setup();

    __disable_interrupt();      // Disable global interrupts

    SetVCore(3);
    Init_Clock();               //Init clocks

    __enable_interrupt();       // enable global interrupts

}

/** Initialize Clock
 Initializes all clocks: ACLK, MCLK, SMCLK.
 */
void Init_Clock(void)
{
    // Desired MCLK frequency
#define MCLK_FREQ_KHZ 24000

    // On board crystals frequencies (in Hz)
#define XT1_FREQ 32768
#define XT2_FREQ 24000000

#define XT1_KHZ XT1_FREQ/1000
#define XT2_KHZ XT2_FREQ/1000

    // Ratio used to set DCO (Digitally Controlled Oscillator)
    // We are setting the FLL reference to 1 MHz (XT2/4)
    // Remember to use the same divider in UCS_initClock
#define MCLK_FLLREF_RATIO MCLK_FREQ_KHZ/(XT2_KHZ/4)

    // Set core power mode
    PMM_setVCore(PMM_CORE_LEVEL_3);

    // Connect Pins to Crystals
    GPIO_setAsPeripheralModuleFunctionInputPin(
    GPIO_PORT_P5,
                                               GPIO_PIN4 + GPIO_PIN2);

    GPIO_setAsPeripheralModuleFunctionOutputPin(
    GPIO_PORT_P5,
                                                GPIO_PIN5 + GPIO_PIN3);

    // Inform the system of the crystal frequencies

    UCS_setExternalClockSource(
    XT1_FREQ,  // Frequency of XT1 in Hz.
            XT2_FREQ   // Frequency of XT2 in Hz.
            );

    // Initialize the crystals
    UCS_turnOnXT2(
    UCS_XT2_DRIVE_24MHZ_32MHZ);

    //UCS_turnOnLFXT1(UCS_XT1_DRIVE_0,UCS_XCAP_3);

    UCS_initClockSignal(
    UCS_FLLREF,  // The reference for Frequency Locked Loop
            UCS_XT2CLK_SELECT,  // Select XT2
            UCS_CLOCK_DIVIDER_4 // FLL ref. will be 1 MHz (4MHz XT2/4)
            );

    UCS_initFLLSettle(
    MCLK_FREQ_KHZ,
                      MCLK_FLLREF_RATIO);

    //We initialize the FLL and wait for it to settle. This will set the DCO (and subsequently, MCLK and SMCLK) to our desired frequency of 20 MHz. Optionally, we can set SMCLK to a lower frequency by using the DCOCLKDIV:
    // Optional: set SMCLK to something else than full speed
    /*
     UCS_initClockSignal(
     UCS_SMCLK,
     UCS_DCOCLKDIV_SELECT,
     UCS_CLOCK_DIVIDER_1
     );
     */

    // Set auxiliary clock
    UCS_initClockSignal(
    UCS_ACLK,
                        UCS_XT1CLK_SELECT,
                        UCS_CLOCK_DIVIDER_1);
}
