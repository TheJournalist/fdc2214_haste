/*
 * modbusRTU.h
 *
 *  Created on: Oct 9, 2018
 *      Author: TheJournalist
 */

#ifndef MODBUSRTU_H_
#define MODBUSRTU_H_

#include <stdint.h>
#include <msp430.h>

extern uint16_t modbusRegisters[200];
extern uint8_t fdcFifoHead;

uint16_t computeCrc(uint8_t data[], uint16_t length);
void modbusStateMachine();




#endif /* MODBUSRTU_H_ */
