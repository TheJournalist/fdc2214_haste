################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../driverlib/msp430f5xx_6xx/adc10_a.c \
../driverlib/msp430f5xx_6xx/adc12_a.c \
../driverlib/msp430f5xx_6xx/aes.c \
../driverlib/msp430f5xx_6xx/battbak.c \
../driverlib/msp430f5xx_6xx/comp_b.c \
../driverlib/msp430f5xx_6xx/crc.c \
../driverlib/msp430f5xx_6xx/dac12_a.c \
../driverlib/msp430f5xx_6xx/dma.c \
../driverlib/msp430f5xx_6xx/eusci_a_spi.c \
../driverlib/msp430f5xx_6xx/eusci_a_uart.c \
../driverlib/msp430f5xx_6xx/eusci_b_i2c.c \
../driverlib/msp430f5xx_6xx/eusci_b_spi.c \
../driverlib/msp430f5xx_6xx/flashctl.c \
../driverlib/msp430f5xx_6xx/gpio.c \
../driverlib/msp430f5xx_6xx/ldopwr.c \
../driverlib/msp430f5xx_6xx/mpy32.c \
../driverlib/msp430f5xx_6xx/pmap.c \
../driverlib/msp430f5xx_6xx/pmm.c \
../driverlib/msp430f5xx_6xx/ram.c \
../driverlib/msp430f5xx_6xx/ref.c \
../driverlib/msp430f5xx_6xx/rtc_a.c \
../driverlib/msp430f5xx_6xx/rtc_b.c \
../driverlib/msp430f5xx_6xx/rtc_c.c \
../driverlib/msp430f5xx_6xx/sd24_b.c \
../driverlib/msp430f5xx_6xx/sfr.c \
../driverlib/msp430f5xx_6xx/sysctl.c \
../driverlib/msp430f5xx_6xx/tec.c \
../driverlib/msp430f5xx_6xx/timer_a.c \
../driverlib/msp430f5xx_6xx/timer_b.c \
../driverlib/msp430f5xx_6xx/timer_d.c \
../driverlib/msp430f5xx_6xx/tlv.c \
../driverlib/msp430f5xx_6xx/ucs.c \
../driverlib/msp430f5xx_6xx/usci_a_spi.c \
../driverlib/msp430f5xx_6xx/usci_a_uart.c \
../driverlib/msp430f5xx_6xx/usci_b_i2c.c \
../driverlib/msp430f5xx_6xx/usci_b_spi.c \
../driverlib/msp430f5xx_6xx/wdt_a.c 

C_DEPS += \
./driverlib/msp430f5xx_6xx/adc10_a.d \
./driverlib/msp430f5xx_6xx/adc12_a.d \
./driverlib/msp430f5xx_6xx/aes.d \
./driverlib/msp430f5xx_6xx/battbak.d \
./driverlib/msp430f5xx_6xx/comp_b.d \
./driverlib/msp430f5xx_6xx/crc.d \
./driverlib/msp430f5xx_6xx/dac12_a.d \
./driverlib/msp430f5xx_6xx/dma.d \
./driverlib/msp430f5xx_6xx/eusci_a_spi.d \
./driverlib/msp430f5xx_6xx/eusci_a_uart.d \
./driverlib/msp430f5xx_6xx/eusci_b_i2c.d \
./driverlib/msp430f5xx_6xx/eusci_b_spi.d \
./driverlib/msp430f5xx_6xx/flashctl.d \
./driverlib/msp430f5xx_6xx/gpio.d \
./driverlib/msp430f5xx_6xx/ldopwr.d \
./driverlib/msp430f5xx_6xx/mpy32.d \
./driverlib/msp430f5xx_6xx/pmap.d \
./driverlib/msp430f5xx_6xx/pmm.d \
./driverlib/msp430f5xx_6xx/ram.d \
./driverlib/msp430f5xx_6xx/ref.d \
./driverlib/msp430f5xx_6xx/rtc_a.d \
./driverlib/msp430f5xx_6xx/rtc_b.d \
./driverlib/msp430f5xx_6xx/rtc_c.d \
./driverlib/msp430f5xx_6xx/sd24_b.d \
./driverlib/msp430f5xx_6xx/sfr.d \
./driverlib/msp430f5xx_6xx/sysctl.d \
./driverlib/msp430f5xx_6xx/tec.d \
./driverlib/msp430f5xx_6xx/timer_a.d \
./driverlib/msp430f5xx_6xx/timer_b.d \
./driverlib/msp430f5xx_6xx/timer_d.d \
./driverlib/msp430f5xx_6xx/tlv.d \
./driverlib/msp430f5xx_6xx/ucs.d \
./driverlib/msp430f5xx_6xx/usci_a_spi.d \
./driverlib/msp430f5xx_6xx/usci_a_uart.d \
./driverlib/msp430f5xx_6xx/usci_b_i2c.d \
./driverlib/msp430f5xx_6xx/usci_b_spi.d \
./driverlib/msp430f5xx_6xx/wdt_a.d 

OBJS += \
./driverlib/msp430f5xx_6xx/adc10_a.obj \
./driverlib/msp430f5xx_6xx/adc12_a.obj \
./driverlib/msp430f5xx_6xx/aes.obj \
./driverlib/msp430f5xx_6xx/battbak.obj \
./driverlib/msp430f5xx_6xx/comp_b.obj \
./driverlib/msp430f5xx_6xx/crc.obj \
./driverlib/msp430f5xx_6xx/dac12_a.obj \
./driverlib/msp430f5xx_6xx/dma.obj \
./driverlib/msp430f5xx_6xx/eusci_a_spi.obj \
./driverlib/msp430f5xx_6xx/eusci_a_uart.obj \
./driverlib/msp430f5xx_6xx/eusci_b_i2c.obj \
./driverlib/msp430f5xx_6xx/eusci_b_spi.obj \
./driverlib/msp430f5xx_6xx/flashctl.obj \
./driverlib/msp430f5xx_6xx/gpio.obj \
./driverlib/msp430f5xx_6xx/ldopwr.obj \
./driverlib/msp430f5xx_6xx/mpy32.obj \
./driverlib/msp430f5xx_6xx/pmap.obj \
./driverlib/msp430f5xx_6xx/pmm.obj \
./driverlib/msp430f5xx_6xx/ram.obj \
./driverlib/msp430f5xx_6xx/ref.obj \
./driverlib/msp430f5xx_6xx/rtc_a.obj \
./driverlib/msp430f5xx_6xx/rtc_b.obj \
./driverlib/msp430f5xx_6xx/rtc_c.obj \
./driverlib/msp430f5xx_6xx/sd24_b.obj \
./driverlib/msp430f5xx_6xx/sfr.obj \
./driverlib/msp430f5xx_6xx/sysctl.obj \
./driverlib/msp430f5xx_6xx/tec.obj \
./driverlib/msp430f5xx_6xx/timer_a.obj \
./driverlib/msp430f5xx_6xx/timer_b.obj \
./driverlib/msp430f5xx_6xx/timer_d.obj \
./driverlib/msp430f5xx_6xx/tlv.obj \
./driverlib/msp430f5xx_6xx/ucs.obj \
./driverlib/msp430f5xx_6xx/usci_a_spi.obj \
./driverlib/msp430f5xx_6xx/usci_a_uart.obj \
./driverlib/msp430f5xx_6xx/usci_b_i2c.obj \
./driverlib/msp430f5xx_6xx/usci_b_spi.obj \
./driverlib/msp430f5xx_6xx/wdt_a.obj 

OBJS__QUOTED += \
"driverlib\msp430f5xx_6xx\adc10_a.obj" \
"driverlib\msp430f5xx_6xx\adc12_a.obj" \
"driverlib\msp430f5xx_6xx\aes.obj" \
"driverlib\msp430f5xx_6xx\battbak.obj" \
"driverlib\msp430f5xx_6xx\comp_b.obj" \
"driverlib\msp430f5xx_6xx\crc.obj" \
"driverlib\msp430f5xx_6xx\dac12_a.obj" \
"driverlib\msp430f5xx_6xx\dma.obj" \
"driverlib\msp430f5xx_6xx\eusci_a_spi.obj" \
"driverlib\msp430f5xx_6xx\eusci_a_uart.obj" \
"driverlib\msp430f5xx_6xx\eusci_b_i2c.obj" \
"driverlib\msp430f5xx_6xx\eusci_b_spi.obj" \
"driverlib\msp430f5xx_6xx\flashctl.obj" \
"driverlib\msp430f5xx_6xx\gpio.obj" \
"driverlib\msp430f5xx_6xx\ldopwr.obj" \
"driverlib\msp430f5xx_6xx\mpy32.obj" \
"driverlib\msp430f5xx_6xx\pmap.obj" \
"driverlib\msp430f5xx_6xx\pmm.obj" \
"driverlib\msp430f5xx_6xx\ram.obj" \
"driverlib\msp430f5xx_6xx\ref.obj" \
"driverlib\msp430f5xx_6xx\rtc_a.obj" \
"driverlib\msp430f5xx_6xx\rtc_b.obj" \
"driverlib\msp430f5xx_6xx\rtc_c.obj" \
"driverlib\msp430f5xx_6xx\sd24_b.obj" \
"driverlib\msp430f5xx_6xx\sfr.obj" \
"driverlib\msp430f5xx_6xx\sysctl.obj" \
"driverlib\msp430f5xx_6xx\tec.obj" \
"driverlib\msp430f5xx_6xx\timer_a.obj" \
"driverlib\msp430f5xx_6xx\timer_b.obj" \
"driverlib\msp430f5xx_6xx\timer_d.obj" \
"driverlib\msp430f5xx_6xx\tlv.obj" \
"driverlib\msp430f5xx_6xx\ucs.obj" \
"driverlib\msp430f5xx_6xx\usci_a_spi.obj" \
"driverlib\msp430f5xx_6xx\usci_a_uart.obj" \
"driverlib\msp430f5xx_6xx\usci_b_i2c.obj" \
"driverlib\msp430f5xx_6xx\usci_b_spi.obj" \
"driverlib\msp430f5xx_6xx\wdt_a.obj" 

C_DEPS__QUOTED += \
"driverlib\msp430f5xx_6xx\adc10_a.d" \
"driverlib\msp430f5xx_6xx\adc12_a.d" \
"driverlib\msp430f5xx_6xx\aes.d" \
"driverlib\msp430f5xx_6xx\battbak.d" \
"driverlib\msp430f5xx_6xx\comp_b.d" \
"driverlib\msp430f5xx_6xx\crc.d" \
"driverlib\msp430f5xx_6xx\dac12_a.d" \
"driverlib\msp430f5xx_6xx\dma.d" \
"driverlib\msp430f5xx_6xx\eusci_a_spi.d" \
"driverlib\msp430f5xx_6xx\eusci_a_uart.d" \
"driverlib\msp430f5xx_6xx\eusci_b_i2c.d" \
"driverlib\msp430f5xx_6xx\eusci_b_spi.d" \
"driverlib\msp430f5xx_6xx\flashctl.d" \
"driverlib\msp430f5xx_6xx\gpio.d" \
"driverlib\msp430f5xx_6xx\ldopwr.d" \
"driverlib\msp430f5xx_6xx\mpy32.d" \
"driverlib\msp430f5xx_6xx\pmap.d" \
"driverlib\msp430f5xx_6xx\pmm.d" \
"driverlib\msp430f5xx_6xx\ram.d" \
"driverlib\msp430f5xx_6xx\ref.d" \
"driverlib\msp430f5xx_6xx\rtc_a.d" \
"driverlib\msp430f5xx_6xx\rtc_b.d" \
"driverlib\msp430f5xx_6xx\rtc_c.d" \
"driverlib\msp430f5xx_6xx\sd24_b.d" \
"driverlib\msp430f5xx_6xx\sfr.d" \
"driverlib\msp430f5xx_6xx\sysctl.d" \
"driverlib\msp430f5xx_6xx\tec.d" \
"driverlib\msp430f5xx_6xx\timer_a.d" \
"driverlib\msp430f5xx_6xx\timer_b.d" \
"driverlib\msp430f5xx_6xx\timer_d.d" \
"driverlib\msp430f5xx_6xx\tlv.d" \
"driverlib\msp430f5xx_6xx\ucs.d" \
"driverlib\msp430f5xx_6xx\usci_a_spi.d" \
"driverlib\msp430f5xx_6xx\usci_a_uart.d" \
"driverlib\msp430f5xx_6xx\usci_b_i2c.d" \
"driverlib\msp430f5xx_6xx\usci_b_spi.d" \
"driverlib\msp430f5xx_6xx\wdt_a.d" 

C_SRCS__QUOTED += \
"../driverlib/msp430f5xx_6xx/adc10_a.c" \
"../driverlib/msp430f5xx_6xx/adc12_a.c" \
"../driverlib/msp430f5xx_6xx/aes.c" \
"../driverlib/msp430f5xx_6xx/battbak.c" \
"../driverlib/msp430f5xx_6xx/comp_b.c" \
"../driverlib/msp430f5xx_6xx/crc.c" \
"../driverlib/msp430f5xx_6xx/dac12_a.c" \
"../driverlib/msp430f5xx_6xx/dma.c" \
"../driverlib/msp430f5xx_6xx/eusci_a_spi.c" \
"../driverlib/msp430f5xx_6xx/eusci_a_uart.c" \
"../driverlib/msp430f5xx_6xx/eusci_b_i2c.c" \
"../driverlib/msp430f5xx_6xx/eusci_b_spi.c" \
"../driverlib/msp430f5xx_6xx/flashctl.c" \
"../driverlib/msp430f5xx_6xx/gpio.c" \
"../driverlib/msp430f5xx_6xx/ldopwr.c" \
"../driverlib/msp430f5xx_6xx/mpy32.c" \
"../driverlib/msp430f5xx_6xx/pmap.c" \
"../driverlib/msp430f5xx_6xx/pmm.c" \
"../driverlib/msp430f5xx_6xx/ram.c" \
"../driverlib/msp430f5xx_6xx/ref.c" \
"../driverlib/msp430f5xx_6xx/rtc_a.c" \
"../driverlib/msp430f5xx_6xx/rtc_b.c" \
"../driverlib/msp430f5xx_6xx/rtc_c.c" \
"../driverlib/msp430f5xx_6xx/sd24_b.c" \
"../driverlib/msp430f5xx_6xx/sfr.c" \
"../driverlib/msp430f5xx_6xx/sysctl.c" \
"../driverlib/msp430f5xx_6xx/tec.c" \
"../driverlib/msp430f5xx_6xx/timer_a.c" \
"../driverlib/msp430f5xx_6xx/timer_b.c" \
"../driverlib/msp430f5xx_6xx/timer_d.c" \
"../driverlib/msp430f5xx_6xx/tlv.c" \
"../driverlib/msp430f5xx_6xx/ucs.c" \
"../driverlib/msp430f5xx_6xx/usci_a_spi.c" \
"../driverlib/msp430f5xx_6xx/usci_a_uart.c" \
"../driverlib/msp430f5xx_6xx/usci_b_i2c.c" \
"../driverlib/msp430f5xx_6xx/usci_b_spi.c" \
"../driverlib/msp430f5xx_6xx/wdt_a.c" 


