################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../library/LDC13xx16xx_evm.c \
../library/crc8.c \
../library/i2c.c \
../library/modbusRTU.c \
../library/pll.c \
../library/spi_1p1.c \
../library/uart.c 

C_DEPS += \
./library/LDC13xx16xx_evm.d \
./library/crc8.d \
./library/i2c.d \
./library/modbusRTU.d \
./library/pll.d \
./library/spi_1p1.d \
./library/uart.d 

OBJS += \
./library/LDC13xx16xx_evm.obj \
./library/crc8.obj \
./library/i2c.obj \
./library/modbusRTU.obj \
./library/pll.obj \
./library/spi_1p1.obj \
./library/uart.obj 

OBJS__QUOTED += \
"library\LDC13xx16xx_evm.obj" \
"library\crc8.obj" \
"library\i2c.obj" \
"library\modbusRTU.obj" \
"library\pll.obj" \
"library\spi_1p1.obj" \
"library\uart.obj" 

C_DEPS__QUOTED += \
"library\LDC13xx16xx_evm.d" \
"library\crc8.d" \
"library\i2c.d" \
"library\modbusRTU.d" \
"library\pll.d" \
"library\spi_1p1.d" \
"library\uart.d" 

C_SRCS__QUOTED += \
"../library/LDC13xx16xx_evm.c" \
"../library/crc8.c" \
"../library/i2c.c" \
"../library/modbusRTU.c" \
"../library/pll.c" \
"../library/spi_1p1.c" \
"../library/uart.c" 


