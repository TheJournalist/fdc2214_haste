#include <intrinsics.h>
#include <string.h>
#include <stdint.h>
#include <cstring>
#include <stdio.h>
#include "LDC13xx16xx_cmd.h"
#include "LDC13xx16xx_evm.h"
#include "driverlib/MSP430F5xx_6xx/driverlib.h"
#include "uart.h"
#include "modbusRTU.h"

uint16_t modbusRegisters[200];
volatile uint8_t dataReady;

uint8_t fdcFifoHead = 0;

void swapc(char* a, char* b)
{
    char temp;

    temp = *b;
    *b = *a;
    *a = temp;

}

void reverse(char str[], uint32_t length)
{
    uint32_t start = 0;
    uint32_t end = length - 1;
    while (start < end)
    {
        swapc((str + start), (str + end));
        start++;
        end--;
    }
}

// Implementation of ultoa()
char* ultoa(uint32_t num, char* str, uint32_t base)
{
    uint32_t i = 0;

    /* Handle 0 explicitely, otherwise empty string is printed for 0 */
    if (num == 0)
    {
        str[i++] = '0';
        str[i] = '\n';
        return str;
    }

    // Process individual digits
    while (num != 0)
    {
        uint32_t rem = num % base;
        str[i++] = (rem > 9) ? (rem - 10) + 'a' : rem + '0';
        num = num / base;
    }

    str[i] = '\n';

    // Reverse the string
    reverse(str, i);

    return str;
}


void main(void)
{
    //Stop watchdog timer
    WDTCTL = WDTPW + WDTHOLD;

    // Read sensor address from flash
    uint8_t *Flash_ptr = (uint8_t *) 0x10000; // Initialize Flash pointer
    FCTL3 = FWKEY;                            // Clear Lock bit
    FCTL1 = FWKEY + WRT;                      // Set WRT bit for write operation
    modbusRegisters[160] = *Flash_ptr;        // Read from Flash
    FCTL1 = FWKEY;                            // Clear WRT bit
    FCTL3 = FWKEY + LOCK;                     // Set LOCK bit

    P1DIR |= BIT0;  // LED
    P1OUT &= ~BIT0;

    // Init MCU
    init_Clock_prePLL();
    InitMCU();
    evm_Delay_Ms(10);

    // Init FDC2214
    while (!fdc2214_init())
        evm_Delay_Ms(10);

    P1OUT |= BIT0;

    uartInit();
    _enable_interrupts();

    while (1)
    {
        modbusStateMachine();

        // Interrupt driven FDC2214 2kHz CH0 reads
        if (dataReady)
        {
            uint8_t ret;
            uint16_t t_buf[2] = { 0x00, 0x00 };

            // Clear interrupt flag
            dataReady = 0;
            // Clear INTB by reading STATUS register
            smbus_readWord(0x2A, LDC13xx16xx_CMD_STATUS, &t_buf[0]);

            // Leitura canal 0
            ret = smbus_readWord(0x2A, LDC13xx16xx_CMD_DATA_MSB_CH0, &t_buf[0]);
            ret &= smbus_readWord(0x2A, LDC13xx16xx_CMD_DATA_LSB_CH0, &t_buf[1]);
            if (ret)
            {
                modbusRegisters[fdcFifoHead++] = t_buf[0] & ~(0xF000); // Hi
                modbusRegisters[fdcFifoHead++] = t_buf[1]; // Lo
                fdcFifoHead = fdcFifoHead % 100;

            }
        }

    }
}

/** Oscillator
 Disables USB if there is a problem with the oscillator
 */
#pragma vector = UNMI_VECTOR
__interrupt void UNMI_ISR(void)
{
    switch (__even_in_range(SYSUNIV, SYSUNIV_BUSIFG))
    {
    case SYSUNIV_NONE:
        __no_operation();
        break;
    case SYSUNIV_NMIIFG:
        __no_operation();
        break;
    case SYSUNIV_OFIFG:
        UCSCTL7 &= ~(DCOFFG + XT1LFOFFG + XT2OFFG); //Clear OSC flaut Flags fault flags
        SFRIFG1 &= ~OFIFG;                          //Clear OFIFG fault flag
        break;
    case SYSUNIV_ACCVIFG:
        __no_operation();
        break;
    case SYSUNIV_BUSIFG:
        SYSBERRIV = 0;                               //clear bus error flag
    }
}
