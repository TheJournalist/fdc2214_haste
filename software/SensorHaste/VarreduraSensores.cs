﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SensorHaste
{
    public partial class VarreduraSensores : Form
    {
        public VarreduraSensores()
        {
            InitializeComponent();
        }

        public void setProgressBar(int i)
        {
            ThreadHelperClass.SetProgressBarValue(this, pbVarredura, i);
        }

        public void closeForm()
        {
            ThreadHelperClass.CloseForm(this);
        }
    }
}
