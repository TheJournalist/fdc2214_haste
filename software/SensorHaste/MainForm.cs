﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.Threading;
using LiveCharts;
using LiveCharts.Configurations;
using LiveCharts.Wpf;
using System.Windows.Media;
using System.Reflection;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace SensorHaste
{
    public partial class MainForm : Form
    {
        public class Crc16
        {
            private static ushort[] CrcTable = {
            0X0000, 0XC0C1, 0XC181, 0X0140, 0XC301, 0X03C0, 0X0280, 0XC241,
            0XC601, 0X06C0, 0X0780, 0XC741, 0X0500, 0XC5C1, 0XC481, 0X0440,
            0XCC01, 0X0CC0, 0X0D80, 0XCD41, 0X0F00, 0XCFC1, 0XCE81, 0X0E40,
            0X0A00, 0XCAC1, 0XCB81, 0X0B40, 0XC901, 0X09C0, 0X0880, 0XC841,
            0XD801, 0X18C0, 0X1980, 0XD941, 0X1B00, 0XDBC1, 0XDA81, 0X1A40,
            0X1E00, 0XDEC1, 0XDF81, 0X1F40, 0XDD01, 0X1DC0, 0X1C80, 0XDC41,
            0X1400, 0XD4C1, 0XD581, 0X1540, 0XD701, 0X17C0, 0X1680, 0XD641,
            0XD201, 0X12C0, 0X1380, 0XD341, 0X1100, 0XD1C1, 0XD081, 0X1040,
            0XF001, 0X30C0, 0X3180, 0XF141, 0X3300, 0XF3C1, 0XF281, 0X3240,
            0X3600, 0XF6C1, 0XF781, 0X3740, 0XF501, 0X35C0, 0X3480, 0XF441,
            0X3C00, 0XFCC1, 0XFD81, 0X3D40, 0XFF01, 0X3FC0, 0X3E80, 0XFE41,
            0XFA01, 0X3AC0, 0X3B80, 0XFB41, 0X3900, 0XF9C1, 0XF881, 0X3840,
            0X2800, 0XE8C1, 0XE981, 0X2940, 0XEB01, 0X2BC0, 0X2A80, 0XEA41,
            0XEE01, 0X2EC0, 0X2F80, 0XEF41, 0X2D00, 0XEDC1, 0XEC81, 0X2C40,
            0XE401, 0X24C0, 0X2580, 0XE541, 0X2700, 0XE7C1, 0XE681, 0X2640,
            0X2200, 0XE2C1, 0XE381, 0X2340, 0XE101, 0X21C0, 0X2080, 0XE041,
            0XA001, 0X60C0, 0X6180, 0XA141, 0X6300, 0XA3C1, 0XA281, 0X6240,
            0X6600, 0XA6C1, 0XA781, 0X6740, 0XA501, 0X65C0, 0X6480, 0XA441,
            0X6C00, 0XACC1, 0XAD81, 0X6D40, 0XAF01, 0X6FC0, 0X6E80, 0XAE41,
            0XAA01, 0X6AC0, 0X6B80, 0XAB41, 0X6900, 0XA9C1, 0XA881, 0X6840,
            0X7800, 0XB8C1, 0XB981, 0X7940, 0XBB01, 0X7BC0, 0X7A80, 0XBA41,
            0XBE01, 0X7EC0, 0X7F80, 0XBF41, 0X7D00, 0XBDC1, 0XBC81, 0X7C40,
            0XB401, 0X74C0, 0X7580, 0XB541, 0X7700, 0XB7C1, 0XB681, 0X7640,
            0X7200, 0XB2C1, 0XB381, 0X7340, 0XB101, 0X71C0, 0X7080, 0XB041,
            0X5000, 0X90C1, 0X9181, 0X5140, 0X9301, 0X53C0, 0X5280, 0X9241,
            0X9601, 0X56C0, 0X5780, 0X9741, 0X5500, 0X95C1, 0X9481, 0X5440,
            0X9C01, 0X5CC0, 0X5D80, 0X9D41, 0X5F00, 0X9FC1, 0X9E81, 0X5E40,
            0X5A00, 0X9AC1, 0X9B81, 0X5B40, 0X9901, 0X59C0, 0X5880, 0X9841,
            0X8801, 0X48C0, 0X4980, 0X8941, 0X4B00, 0X8BC1, 0X8A81, 0X4A40,
            0X4E00, 0X8EC1, 0X8F81, 0X4F40, 0X8D01, 0X4DC0, 0X4C80, 0X8C41,
            0X4400, 0X84C1, 0X8581, 0X4540, 0X8701, 0X47C0, 0X4680, 0X8641,
            0X8201, 0X42C0, 0X4380, 0X8341, 0X4100, 0X81C1, 0X8081, 0X4040 };

            public static UInt16 ComputeCrc(byte[] data)
            {
                ushort crc = 0xFFFF;

                foreach (byte datum in data)
                {
                    crc = (ushort)((crc >> 8) ^ CrcTable[(crc ^ datum) & 0xFF]);
                }

                return crc;
            }
        }

        public class MeasureModel
        {
            public System.DateTime DateTime { get; set; }
            public double Value { get; set; }
        }

        // Porta serial
        static SerialPort serialPort = new SerialPort();
        private bool btnOpenCloseEnabled = false;
        bool closePort = false;
        byte[] lastBytes = new byte[205];
        byte[] crcBytes = new byte[203];
        bool messageReceived = false;
        int messagesReceived;

        // Varredura sensores
        bool flagVarredura;
        public int varredura;

        // Aquisição
        string filename;
        Queue<int> sensorMessage = new Queue<int>();

        // Sensores
        List<Sensor> sensores = new List<Sensor>();

        // Aquisição
        Thread ThrAq;
        bool flagAq = false;
        int tempoAq;
        int tempoTaxa;
        double stepDuration;
        long freq;
        double ticksPerStep;

        class Sensor
        {
            /// <summary>
            /// ID do sensor, endereço, valor entre 1 e 247
            /// </summary>
            public byte id;

            /// <summary>
            /// Valor lido do ADC
            /// </summary>
            public UInt32 modulo;

            /// <summary>
            /// Frequência de excitação do sinal (100 - 1000) [kHz]
            /// </summary>
            public int freqVref;

            /// <summary>
            /// Frequência de excitação do sinal (100 - 1000) [kHz]
            /// </summary>
            public int temperatura;

            /// <summary>
            /// Frequencia com que o sinal do transdutor é lido 
            /// pelo microcontrolador(1 a 2000 amostras/s)
            /// </summary>
            public int freqAmostragem;

            /// <summary>
            /// O modelo elétrico corresponde a um resistor em série com o sensor.
            /// O valor deste resistor pode ser alterado
            /// tendo as opções de 100 ohms e 3300 ohms
            /// </summary>
            public int resCarga;

            /// <summary>
            /// O modelo elétrico corresponde a um resistor em série com o sensor.
            /// O valor deste resistor pode ser alterado
            /// tendo as opções de 100 ohms e 3300 ohms
            /// </summary>
            public System.Windows.Media.SolidColorBrush color;

            /// <summary>
            /// Leituras da aquisição
            /// </summary>
            public List<UInt32> aquisicao;


            public ChartValues<MeasureModel> chartValues { get; set; }

            public Sensor(byte _id, SeriesCollection series)
            {
                id = _id;

                aquisicao = new List<UInt32>();

                var mapper = Mappers.Xy<MeasureModel>()
                .X(model => model.DateTime.Ticks)   // DateTime.Ticks as X
                .Y(model => model.Value);           // value property as Y

                Charting.For<MeasureModel>(mapper);
                chartValues = new ChartValues<MeasureModel>();

                Random rnd = new Random();
                color = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromRgb((byte)rnd.Next(30, 256), (byte)rnd.Next(30, 256), (byte)rnd.Next(30, 256)));

                series.Add(
                    new LineSeries
                    {
                        Title = "Sensor " + id.ToString(),
                        Values = chartValues,
                        PointGeometrySize = 4,
                        StrokeThickness = 2,
                        Stroke = color,
                        Fill = System.Windows.Media.Brushes.Transparent,
                        PointForeground = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromRgb(30, 30, 30))
                    }
                );
            }

            public void addSample(UInt32 valor)
            {
                this.modulo = valor;

                var now = System.DateTime.Now;

                chartValues.Add(new MeasureModel
                {
                    DateTime = now,
                    Value = valor
                });

                // Mantem apenas os 200 últimos valores
                if (chartValues.Count > 50)
                    chartValues.RemoveAt(0);
            }
        }

        // Chart
        public System.Windows.Forms.Timer Timer { get; set; }

        // Inicialização
        public MainForm()
        {
            InitializeComponent();

            // Diretório atual
            tbSave.Text = System.AppDomain.CurrentDomain.BaseDirectory;
            filename = tbSave.Text;

            // Lê portas seriais disponíveis
            foreach (var port in SerialPort.GetPortNames())
                cbCOM.Items.Add(port);

            // Thread de recepção da serial
            serialPort.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler);

            // Pré-seleção das comboBox
            if (cbCOM.Items.Count > 0)
                cbCOM.SelectedIndex = 0;
            cbBaud.SelectedIndex = 0;
            cbBaudSensor.SelectedIndex = 0;

            // Thread de aquisição
            ThrAq = new Thread(threadAquisicao);
            ThrAq.Start();


            // Inicializa gráfico
            var mapper = Mappers.Xy<MeasureModel>()
                .X(model => model.DateTime.Ticks)   // DateTime.Ticks as X
                .Y(model => model.Value);           // value property as Y

            Charting.For<MeasureModel>(mapper);

            cartesianChart1.Series = new SeriesCollection();
            cartesianChart1.AxisX.Add(new Axis
            {
                DisableAnimations = false,
                LabelFormatter = value => new System.DateTime((long)value).ToString("ss"),
                Separator = new Separator
                {
                    StrokeThickness = 0,
                    StrokeDashArray = new System.Windows.Media.DoubleCollection(new double[] { 4 }),
                    Stroke = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromRgb(64, 79, 86)),
                    Step = TimeSpan.FromSeconds(1).Ticks
                },
            });

            cartesianChart1.AxisY.Add(new Axis
            {
                IsMerged = false,
                FontSize = 7,
                Separator = new Separator
                {
                    StrokeThickness = 0.1,
                    StrokeDashArray = new System.Windows.Media.DoubleCollection(4),
                    Stroke = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromRgb(64, 79, 86)),
                    Step = 100000000
                }
            });

            SetAxisLimits(System.DateTime.Now);
            cartesianChart1.AxisY[0].MaxValue = 4294967296;
            cartesianChart1.AxisY[0].MinValue = 0;
            cartesianChart1.DataTooltip.IsEnabled = true;
            cartesianChart1.Hoverable = true;
            //cartesianChart1.Zoom = ZoomingOptions.X;

            Timer = new System.Windows.Forms.Timer
            {
                Interval = 200
            };
            Timer.Tick += TimerOnTick;
            Timer.Start();

        }

        public void DataReceivedHandler(object sender, SerialDataReceivedEventArgs e)
        {
            if (flagAq)
            {
                UInt32 leitura = 0;

                while(serialPort.BytesToRead > 0)
                {
                    // Move array para a esquerda
                    Array.Copy(lastBytes, 1, lastBytes, 0, lastBytes.Length - 1);
                    // Novo byte
                    lastBytes[204] = (byte)serialPort.ReadByte();
                    // CRC
                    Array.Copy(lastBytes, 0, crcBytes, 0, lastBytes.Length - 2);

                    int crcC = Crc16.ComputeCrc(crcBytes);
                    int crcL = (lastBytes[204] << 8 | lastBytes[203]);

                    // Valida mensagem através do CRC
                    if (crcC == crcL)
                    {
                        Sensor sensor = sensores.Find(s => s.id == lastBytes[0]);
                        if (sensor != null)
                        {
                            for (int i = 0; i < 50; i++)
                            {
                                leitura = Convert.ToUInt32(((UInt32)lastBytes[3 + 4*i] << 24) | ((UInt32)lastBytes[4 + 4 * i] << 16) | ((UInt32)lastBytes[5 + 4 * i] << 8) | ((UInt32)lastBytes[6 + 4 * i]));
                                sensor.aquisicao.Add(leitura);
                            }
                        }
                        messageReceived = true;
                        messagesReceived++;
                    }
                }
            }
        }


        public void threadAquisicao()
        {
            Stopwatch swTempo = new Stopwatch();
            Stopwatch swTaxa = new Stopwatch();
            Stopwatch swSend = new Stopwatch();
            Stopwatch swTimeout = new Stopwatch();

            int messagesSent = 0;

            while (true)
            {
                if (flagAq)
                {
                    messagesSent = 0;
                    foreach (var item in cbSensores.CheckedItems)
                    {
                        BeginInvoke(new Action(() =>
                        {
                            Sensor sensor = sensores.Find(s => s.id.ToString() == item.ToString());
                            if (sensor != null)
                                sensor.aquisicao.Clear();

                        }));
                    }

                    swTempo.Reset();
                    swTempo.Start();
                    swTaxa.Reset();
                    swTaxa.Start();

                    while (swTempo.ElapsedMilliseconds < tempoAq)
                    {
                        while (swTaxa.ElapsedTicks < ticksPerStep) ;

                        swTaxa.Reset();
                        swTaxa.Start();

                        BeginInvoke(new Action(() =>
                        {
                            foreach (var item in cbSensores.CheckedItems)
                            {
                                if (swTempo.ElapsedMilliseconds > tempoAq)
                                    break;

                                byte endereco = Convert.ToByte(item.ToString());
                                UInt16 CRC16 = Crc16.ComputeCrc(new byte[] { endereco, 0x03, 0x00, 0x00, 0x00, 0x64 });
                                messageReceived = false;
                                serialPort.Write(new byte[] { endereco, 0x03, 0x00, 0x00, 0x00, 0x64, (byte)CRC16, (byte)(CRC16 >> 8) }, 0, 8);

                                if (swTempo.ElapsedMilliseconds > tempoAq)
                                    break;

                                swTimeout.Reset();
                                swTimeout.Start();
                                messagesSent++;

                                while (swTimeout.ElapsedMilliseconds < 2) ;

                                if (swTempo.ElapsedMilliseconds > tempoAq)
                                    break;

                                /*
                                while (swTimeout.ElapsedMilliseconds < 3 && (!messageReceived || swTimeout.ElapsedMilliseconds < 2)) ;
                                    if (swTempo.ElapsedMilliseconds > tempoAq)
                                        break;
                                */


                                /*
                                while (!messageReceived);
                                */
                            }

                        }));

                    }


                    flagAq = false;

                    MessageBox.Show("Fim da aquisição - " + messagesSent.ToString() + "/" + messagesReceived.ToString(), "Aquisição");

                    // Escreve arquivo .csv
                    using (StreamWriter w = File.AppendText(filename + "\\Experimento-" + DateTime.Now.ToString(@"yyyy-MM-dd--HH-mm-ss") + ".csv"))
                    {

                        foreach (var item in cbSensores.CheckedItems)
                        {

                            Sensor sensor = sensores.Find(s => s.id.ToString() == item.ToString());
                            if (sensor != null)
                            {
                                w.Write("Sensor " + sensor.id.ToString());

                                if (Convert.ToInt16(sensor.id.ToString()) == Convert.ToInt16(cbSensores.CheckedItems[cbSensores.CheckedItems.Count - 1].ToString()))
                                    w.Write("\n");
                                else
                                    w.Write(",");
                            }
                        }


                        foreach (var item in cbSensores.CheckedItems)
                        {
                            Sensor sensor = sensores.Find(s => s.id.ToString() == item.ToString());
                            if (sensor != null)
                            {
                                for (int i = 0; i < messagesReceived * 50; i++)
                                {
                                    w.Write(sensor.aquisicao.ElementAtOrDefault(i));

                                    if (Convert.ToInt16(sensor.id.ToString()) == Convert.ToInt16(cbSensores.CheckedItems[cbSensores.CheckedItems.Count - 1].ToString()))
                                        w.Write("\n");
                                    else
                                        w.Write(",");
                                }
                            }

                        }
                    }
                }
            }
        }

        private void SetAxisLimits(System.DateTime now)
        {
            cartesianChart1.AxisX[0].MaxValue = now.Ticks + TimeSpan.FromSeconds(1).Ticks;
            cartesianChart1.AxisX[0].MinValue = now.Ticks - TimeSpan.FromSeconds(10).Ticks; // 10 segundos no gráfico
        }

        private void TimerOnTick(object sender, EventArgs eventArgs)
        {
            if (cbChart.Checked)
            {
                if (serialPort.IsOpen)
                {
                    foreach (var item in cbSensores.CheckedItems)
                    {
                        byte endereco = Convert.ToByte(item.ToString());
                        UInt16 CRC16 = Crc16.ComputeCrc(new byte[] { endereco, 0x03, 0x00, 0x00, 0x00, 0x02 });
                        serialPort.Write(new byte[] { endereco, 0x03, 0x00, 0x00, 0x00, 0x02, (byte)CRC16, (byte)(CRC16 >> 8) }, 0, 8);
                        Stopwatch stopwatch = new Stopwatch();
                        stopwatch.Start();
                        UInt32 leitura = 0;
                        UInt32 id = 255;

                        while (stopwatch.Elapsed < TimeSpan.FromMilliseconds(20))
                        {
                            if (serialPort.BytesToRead > 8)
                            {
                                UInt32 sByte = 0;
                                id = Convert.ToUInt32(serialPort.ReadByte());
                                sByte = Convert.ToUInt32(serialPort.ReadByte());
                                sByte = Convert.ToUInt32(serialPort.ReadByte());

                                sByte = Convert.ToUInt32(serialPort.ReadByte());
                                leitura |= sByte << 24;
                                sByte = Convert.ToUInt32(serialPort.ReadByte());
                                leitura |= sByte << 16;

                                sByte = Convert.ToUInt32(serialPort.ReadByte());
                                leitura |= sByte << 8;
                                sByte = Convert.ToUInt32(serialPort.ReadByte());
                                leitura |= sByte;

                                sByte = Convert.ToUInt32(serialPort.ReadByte());
                                sByte = Convert.ToUInt32(serialPort.ReadByte());
                                break;
                            }
                        }
                        stopwatch.Stop();

                        Sensor sensor = sensores.Find(s => s.id == id);
                        if (sensor != null)
                            sensor.addSample(leitura);

                        SetAxisLimits(System.DateTime.Now);
                    }
                }
            }
        }

        // Evento clique do botão de abrir/fechar porta serial
        private void btnOpenClose_Click(object sender, EventArgs e)
        {
            if (btnOpenCloseEnabled)
            {
                if (closePort == false)
                {
                    serialPort.Close();

                    if (serialPort.IsOpen)
                    {
                        MessageBox.Show("Erro", "Falha ao fechar " + cbCOM.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        cbCOM.Enabled = true;
                        cbBaud.Enabled = true;
                        closePort = true;
                        btnOpenClose.Text = "Abrir";
                    }
                }
                else
                {
                    try
                    {
                        serialPort.PortName = cbCOM.SelectedItem.ToString();
                        serialPort.BaudRate = Convert.ToInt32(cbBaud.SelectedItem.ToString());
                        serialPort.Parity = Parity.None;
                        serialPort.StopBits = StopBits.One;
                        serialPort.DataBits = 8;
                        serialPort.Handshake = Handshake.None;
                        serialPort.ReadTimeout = 100;
                        serialPort.WriteTimeout = 50;
                        serialPort.ReadBufferSize = 4096;
                        serialPort.WriteBufferSize = 4096;
                        serialPort.RtsEnable = true;
                        serialPort.Open();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Falha ao abrir " + cbCOM.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    if (serialPort.IsOpen)
                    {
                        cbCOM.Enabled = false;
                        cbBaud.Enabled = false;
                        closePort = false;
                        btnOpenClose.Text = "Fechar";
                    }
                }
            }
        }

        // Evento de seleção de uma porta serial da lista
        private void cbCOM_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (((ComboBox)sender).SelectedIndex == -1)
                btnOpenCloseEnabled = false;
            else
                btnOpenCloseEnabled = true;
        }

        // Botão escreve endereço
        private void btnEndereco_Click(object sender, EventArgs e)
        {
            sendMessage(160, tbEndereco, "Endereço", false, 250);
        }

        // Botão leitura temperatura
        private void btnTemp_Click(object sender, EventArgs e)
        {
            sendMessage(7, lbTemp, "Temperatura", true);
        }

        // Botão leitura módulo
        private void btnModulo_Click(object sender, EventArgs e)
        {
            sendMessage(0, lbModulo, "Módulo Relativo", true);
        }

        // Botão leitura frequência de excitação
        private void btnLerFreqExc_Click(object sender, EventArgs e)
        {
            sendMessage(163, tbFreqExc, "Frequência de Excitação", true);
        }

        // Botão escreve frequência de excitação
        private void btnEscreverFreqExc_Click(object sender, EventArgs e)
        {
            sendMessage(163, tbFreqExc, "Frequência de Excitação", false);
        }

        // Botão leitura frequência de amostragem
        private void btnLerFreqAmos_Click(object sender, EventArgs e)
        {
            sendMessage(167, tbFreqAmos, "Frequência de Amostragem", true);
        }

        // Botão escreve frequência de amostragem
        private void btnEscreverFreqAmos_Click(object sender, EventArgs e)
        {
            sendMessage(167, tbFreqAmos, "Frequência de Amostragem", false);
        }

        // Botão leitura resistência de carga
        private void btnLerRes_Click(object sender, EventArgs e)
        {
            sendMessage(166, tbRes, "Resistência de carga", true);
        }

        // Botão escrita resistência de carga
        private void btnEscreverRes_Click(object sender, EventArgs e)
        {
            sendMessage(166, tbRes, "Resistência de carga", false);
        }

        // Mensagens de leitura e escrita de registradores, atualiza campo na janela com valor lido
        private void sendMessage(byte register, Control field, String description, bool read, byte address = 0x00)
        {
            if (serialPort.IsOpen)
            {
                UInt16 value = 0;
                if (!read)
                {
                    try
                    {
                        if (field == cbBaudSensor)
                            value = Convert.ToUInt16(cbBaudSensor.SelectedIndex + 1);
                        else
                        {
                            value = Convert.ToUInt16(field.Text);
                            field.Text = "";
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Valor inválido!", "Erro - " + description, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }

                if (address == 0x00)
                {
                    try
                    {
                        address = Convert.ToByte(tbSensor.Text);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Endereço de sensor inválido!", "Erro - Endereço", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }

                if(read)
                {
                    UInt16 CRC16 = Crc16.ComputeCrc(new byte[] { address, 0x03, 0x00, register, 0x00, 0x01 });
                    serialPort.Write(new byte[] { address, 0x03, 0x00, register, 0x00, 0x01, (byte)CRC16, (byte)(CRC16 >> 8) }, 0, 8);
                }
                else
                {
                    UInt16 CRC16 = Crc16.ComputeCrc(new byte[] { address, 0x06, 0x00, register, (byte)(value >> 8), (byte)value });
                    serialPort.Write(new byte[] { address, 0x06, 0x00, register, (byte)(value >> 8), (byte)value, (byte)CRC16, (byte)(CRC16 >> 8) }, 0, 8);
                }


                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();
                bool messageReceived = false;
                UInt16 receivedValue = 0;

                while (stopwatch.Elapsed < TimeSpan.FromMilliseconds(500))
                {
                    // Read
                    if (serialPort.BytesToRead == 7)
                    {
                        messageReceived = true;
                        byte sByte;
                        sByte = (byte)serialPort.ReadByte();
                        sByte = (byte)serialPort.ReadByte();
                        sByte = (byte)serialPort.ReadByte();
                        sByte = (byte)serialPort.ReadByte();
                        receivedValue += Convert.ToUInt16(sByte << 8);
                        sByte = (byte)serialPort.ReadByte();
                        receivedValue += Convert.ToUInt16(sByte);
                        serialPort.DiscardInBuffer();
                        break;
                    }

                    // Write
                    if (serialPort.BytesToRead == 8)
                    {
                        messageReceived = true;
                        serialPort.DiscardInBuffer();
                        break;
                    }
                }
                stopwatch.Stop();

                if (!messageReceived)
                    MessageBox.Show("Timeout! O sensor está conectado?", "Erro - " + description, MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                    if (read)
                        if(field != lbTemp)
                            field.Text = receivedValue.ToString();
                        else
                            field.Text = receivedValue.ToString() + "ºC";
                    else
                        MessageBox.Show("Valor definido com sucesso!", "Sucesso - " + description, MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            else
            {
                MessageBox.Show("Porta serial fechada!", "Erro - Porta serial", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public static void AppendText(RichTextBox box, string text, System.Windows.Media.Color color)
        {
            box.SelectionStart = box.TextLength;
            box.SelectionLength = 0;

            box.SelectionColor = System.Drawing.Color.FromArgb(color.R, color.G, color.B);
            box.AppendText(text);
            box.SelectionColor = box.ForeColor;
        }

        private void btnDetectar_Click(object sender, EventArgs e)
        {
            VarreduraSensores formVarredura = new VarreduraSensores();

            new Thread(() => formVarredura.ShowDialog()).Start();

            cbSensores.Items.Clear();
            sensores.Clear();
            rtbLegend.Text = "";

            flagVarredura = true;
            for(int i=0; i<20; i++) //247
            {
                formVarredura.setProgressBar(i);
                UInt16 CRC16 = Crc16.ComputeCrc(new byte[] { (byte)i, 0x03, 0x00, 0x00, 0x00, 0x01 });
                serialPort.Write(new byte[] { (byte)i, 0x03, 0x00, 0x00, 0x00, 0x01, (byte)CRC16, (byte)(CRC16 >> 8) }, 0, 8);

                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();

                while (stopwatch.Elapsed < TimeSpan.FromMilliseconds(100))
                {
                    if(serialPort.BytesToRead >= 7)
                    {
                        // ID: Primeiro byte da mensagem de resposta
                        byte id = (byte)serialPort.ReadByte();
                        serialPort.DiscardInBuffer();
                        sensores.Add(new Sensor(id, cartesianChart1.Series));
                        cbSensores.Items.Add(id);
                        Sensor sensor = sensores.Find(s => s.id == id);
                        AppendText(rtbLegend, "⏺", sensor.color.Color);
                        AppendText(rtbLegend, " Sensor " + id.ToString() + "\n", System.Windows.Media.Color.FromRgb(0,0,0));
                        break;
                    }
                }
                stopwatch.Stop();
            }
            formVarredura.closeForm();
            flagVarredura = false;
        }

        private void btnDiretorio_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.SelectedPath = System.IO.Path.GetFullPath(tbSave.Text);
            DialogResult result = fbd.ShowDialog();
            tbSave.Text = fbd.SelectedPath;
            filename = fbd.SelectedPath;
        }

        private void btnIniciar_Click(object sender, EventArgs e)
        {
            tempoTaxa = Convert.ToInt32(((1.0 / Convert.ToInt16(tbTaxa.Text))*1000)*50);
            tempoAq = Convert.ToInt32(tbTempo.Text) * 1000;

            stepDuration = (1.0 / Convert.ToInt16(tbTaxa.Text)) * 50;
            freq = Stopwatch.Frequency;
            ticksPerStep = (double)freq * stepDuration;
            messagesReceived = 0;
            flagAq = true;
        }

        private void btnEscreveBaud_Click(object sender, EventArgs e)
        {
            sendMessage(161, cbBaudSensor, "Baudrate", false);
        }

        private void btnReg_Click(object sender, EventArgs e)
        {
            sendMessage(Convert.ToByte(tbReg.Text), lbReg, "Registrador", true);
        }

        private void btnRegistradores_Click(object sender, EventArgs e)
        {
            if (serialPort.IsOpen)
            {
                UInt16 value = 0;
                byte address;

                try
                {
                    address = Convert.ToByte(tbSensor.Text);
                }
                catch (Exception)
                {
                    MessageBox.Show("Endereço de sensor inválido!", "Erro - Endereço", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                var csv = new StringBuilder();

                for (byte i = 0; i < 255; i++)
                {

                    UInt16 CRC16 = Crc16.ComputeCrc(new byte[] { address, 0x03, 0x00, i, 0x00, 0x01 });
                    serialPort.Write(new byte[] { address, 0x03, 0x00, i, 0x00, 0x01, (byte)CRC16, (byte)(CRC16 >> 8) }, 0, 8);


                    Stopwatch stopwatch = new Stopwatch();
                    stopwatch.Start();
                    bool messageReceived = false;
                    UInt16 receivedValue = 0;

                    while (stopwatch.Elapsed < TimeSpan.FromMilliseconds(100))
                    {
                        // Read
                        if (serialPort.BytesToRead == 7)
                        {
                            messageReceived = true;
                            byte sByte;
                            sByte = (byte)serialPort.ReadByte();
                            sByte = (byte)serialPort.ReadByte();
                            sByte = (byte)serialPort.ReadByte();
                            sByte = (byte)serialPort.ReadByte();
                            receivedValue += Convert.ToUInt16(sByte << 8);
                            sByte = (byte)serialPort.ReadByte();
                            receivedValue += Convert.ToUInt16(sByte);
                            serialPort.DiscardInBuffer();
                            break;
                        }

                        // Write
                        if (serialPort.BytesToRead == 8)
                        {
                            messageReceived = true;
                            serialPort.DiscardInBuffer();
                            break;
                        }
                    }
                    stopwatch.Stop();

                    if (messageReceived)
                        csv.AppendLine(i.ToString() + ", " + receivedValue);
                    else
                        csv.AppendLine(i.ToString() + ", -1");
                }

                File.WriteAllText(@"regsSensor" + address.ToString() + ".csv", csv.ToString());
            }
        }

        private void btnEscreveReg_Click(object sender, EventArgs e)
        {
            sendMessage(Convert.ToByte(tbReg.Text), tbVReg, "Registrador", false);
        }

        private void btnEscreveRegs_Click(object sender, EventArgs e)
        {
            if (serialPort.IsOpen)
            {
                UInt16 value = 0;
                byte address;

                try
                {
                    address = Convert.ToByte(tbSensor.Text);
                }
                catch (Exception)
                {
                    MessageBox.Show("Endereço de sensor inválido!", "Erro - Endereço", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                UInt16[] regsSensor8 = { 0, 2500, 0, 0, 0, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 63, 46, 100, 20, 95, 0, 0, 0, 100, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 8, 3, 0, 1000, 20, 255, 3300, 2000, 10, 700, 2, 0, 1, 10, 175, 43690, 1, 2000, 0, 0, 0 };
                List<int> notReceived = new List<int>();

                for (byte i = 0; i < 80; i++)
                {
                    // Exceto registradores de EndSensor e BaudRate
                    if (i == (160-100) || i == (161-100))
                        continue;

                    value = regsSensor8[i];

                    UInt16 CRC16 = Crc16.ComputeCrc(new byte[] { address, 0x06, 0x00, Convert.ToByte(100 + i), (byte)(value >> 8), (byte)value });
                    serialPort.Write(new byte[] { address, 0x06, 0x00, Convert.ToByte(100 + i), (byte)(value >> 8), (byte)value, (byte)CRC16, (byte)(CRC16 >> 8) }, 0, 8);


                    Stopwatch stopwatch = new Stopwatch();
                    stopwatch.Start();
                    bool messageReceived = false;
                    UInt16 receivedValue = 0;

                    while (stopwatch.Elapsed < TimeSpan.FromMilliseconds(200))
                    {
                        // Write
                        if (serialPort.BytesToRead >= 8)
                        {
                            byte[] message = new byte[8];

                            for(int j = 0; j < 8; j++)
                                message[j] = (byte)serialPort.ReadByte();

                            messageReceived = true;
                            serialPort.DiscardInBuffer();
                            break;
                        }
                    }
                    stopwatch.Stop();

                    if (!messageReceived)
                        notReceived.Add(100 + i);

                }

                ;
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            System.Environment.Exit(1);
        }
    }
}



public static class ThreadHelperClass
{
    delegate void SetTextCallback(Form f, Control ctrl, string text);
    delegate void SetProgressBarValueCallback(Form f, ProgressBar pb, int value);
    delegate void CloseFormCallback(Form f);
    /// <summary>
    /// Set text property of various controls
    /// </summary>
    /// <param name="form">The calling form</param>
    /// <param name="ctrl"></param>
    /// <param name="text"></param>
    public static void SetText(Form form, Control ctrl, string text)
    {
        // InvokeRequired required compares the thread ID of the 
        // calling thread to the thread ID of the creating thread. 
        // If these threads are different, it returns true. 
        if (ctrl.InvokeRequired)
        {
            SetTextCallback d = new SetTextCallback(SetText);
            form.Invoke(d, new object[] { form, ctrl, text });
        }
        else
        {
            ctrl.Text = text;
        }
    }

    public static void SetProgressBarValue(Form form, ProgressBar pb, int value)
    {
        // InvokeRequired required compares the thread ID of the 
        // calling thread to the thread ID of the creating thread. 
        // If these threads are different, it returns true. 
        if (pb.InvokeRequired)
        {
            SetProgressBarValueCallback d = new SetProgressBarValueCallback(SetProgressBarValue);
            form.Invoke(d, new object[] { form, pb, value });
        }
        else
        {
            pb.Value = value;
        }
    }

    public static void CloseForm(Form form)
    {
        // InvokeRequired required compares the thread ID of the 
        // calling thread to the thread ID of the creating thread. 
        // If these threads are different, it returns true. 
        if (form.InvokeRequired)
        {
            CloseFormCallback d = new CloseFormCallback(CloseForm);
            form.Invoke(d, new object[] { form });
        }
        else
        {
            form.Close();
        }
    }
}